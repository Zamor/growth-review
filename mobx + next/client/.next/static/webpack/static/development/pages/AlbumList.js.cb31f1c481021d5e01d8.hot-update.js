webpackHotUpdate("static/development/pages/AlbumList.js",{

/***/ "./pages/AlbumList/AlbumList.jsx":
/*!***************************************!*\
  !*** ./pages/AlbumList/AlbumList.jsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process) {/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "../node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "../node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/values */ "../node_modules/@babel/runtime-corejs2/core-js/object/values.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "../node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "../node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "../node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "../node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "../node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! prop-types */ "../node_modules/prop-types/index.js");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! styled-components */ "../node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! mobx-react */ "../node_modules/mobx-react/dist/mobx-react.module.js");
/* harmony import */ var _components_Container__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../components/Container */ "./components/Container.jsx");
/* harmony import */ var _components_ListItem__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../components/ListItem */ "./components/ListItem.jsx");
/* harmony import */ var _components_Title__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../components/Title */ "./components/Title.js");









var _dec, _class, _temp;

var __jsx = react__WEBPACK_IMPORTED_MODULE_8___default.a.createElement;







var AlbumList = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_11__["inject"])('store'), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_11__["observer"])(_class = (_temp =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_7__["default"])(AlbumList, _Component);

  function AlbumList() {
    var _getPrototypeOf2;

    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, AlbumList);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, (_getPrototypeOf2 = Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(AlbumList)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _this.renderAlbumList = function () {
      var _this$props$store = _this.props.store,
          authors = _this$props$store.authors,
          albums = _this$props$store.albums;

      var albumsArray = albums && _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_2___default()(albums);

      if (!albumsArray || !albumsArray.length) {
        return null;
      }

      return albumsArray.map(function (singleAlbum) {
        var id = singleAlbum.id,
            title = singleAlbum.title;
        var authorName = authors[singleAlbum.userId] && authors[singleAlbum.userId].name;
        return __jsx(_components_ListItem__WEBPACK_IMPORTED_MODULE_13__["default"], {
          key: "album-".concat(id),
          title: title,
          author: authorName,
          albumId: id
        });
      });
    };

    _this.handleLoad = function () {
      return _this.props.store.fetchAlbums();
    };

    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(AlbumList, [{
    key: "render",
    value: function render() {
      var _this$props$store2 = this.props.store,
          loading = _this$props$store2.loading,
          pageNumber = _this$props$store2.pageNumber;
      var albumList = this.renderAlbumList();
      console.log(process);
      return __jsx(AlbumContainer, null, __jsx(_components_Title__WEBPACK_IMPORTED_MODULE_14__["default"], {
        title: "Available albums"
      }), __jsx(AlbumListWrapper, null, albumList || 'Brak albumów do wyświetlenia'), loading && 'wczytywanie', albumList && pageNumber > 0 && __jsx(LoadMoreButton, {
        onClick: this.handleLoad
      }, "Load More"));
    }
  }], [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
        var store, _page;

        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                store = _ref.store;
                _page = store._page;

                if (_page) {
                  _context.next = 5;
                  break;
                }

                _context.next = 5;
                return store.fetchAlbums();

              case 5:
                return _context.abrupt("return", {});

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getInitialProps(_x) {
        return _getInitialProps.apply(this, arguments);
      }

      return getInitialProps;
    }()
  }]);

  return AlbumList;
}(react__WEBPACK_IMPORTED_MODULE_8__["Component"]), _temp)) || _class) || _class);
var AlbumContainer = Object(styled_components__WEBPACK_IMPORTED_MODULE_10__["default"])(_components_Container__WEBPACK_IMPORTED_MODULE_12__["default"]).withConfig({
  displayName: "AlbumList__AlbumContainer",
  componentId: "sc-3i70r2-0"
})(["margin-top:30px;margin-bottom:10px;display:flex;flex-direction:column;"]);
var AlbumListWrapper = styled_components__WEBPACK_IMPORTED_MODULE_10__["default"].div.withConfig({
  displayName: "AlbumList__AlbumListWrapper",
  componentId: "sc-3i70r2-1"
})(["display:flex;flex-wrap:wrap;margin-top:10px;"]);
var LoadMoreButton = styled_components__WEBPACK_IMPORTED_MODULE_10__["default"].button.withConfig({
  displayName: "AlbumList__LoadMoreButton",
  componentId: "sc-3i70r2-2"
})(["border:1px solid ", ";color:", ";padding:10px 50px;background-color:", ";border-radius:20px;align-self:center;margin-top:15px;:hover{cursor:pointer;}"], function (props) {
  return props.theme.colors.orange;
}, function (props) {
  return props.theme.colors.orange;
}, function (props) {
  return props.theme.colors.white;
});
AlbumList.propTypes = {
  store: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.shape({
    loading: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.bool,
    albums: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.objectOf(prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.shape({
      title: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.string,
      id: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.number
    })),
    authors: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.objectOf(prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.shape({
      name: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.string
    })),
    fetchAlbums: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.func,
    pageNumber: prop_types__WEBPACK_IMPORTED_MODULE_9___default.a.number
  })
};
/* harmony default export */ __webpack_exports__["default"] = (AlbumList);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/process/browser.js */ "../node_modules/process/browser.js")))

/***/ })

})
//# sourceMappingURL=AlbumList.js.cb31f1c481021d5e01d8.hot-update.js.map