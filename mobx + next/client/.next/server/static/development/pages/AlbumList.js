module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/@babel/runtime-corejs2/core-js/object/values.js":
/*!***********************************************************************!*\
  !*** ../node_modules/@babel/runtime-corejs2/core-js/object/values.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/values */ "core-js/library/fn/object/values");

/***/ }),

/***/ "./components/Container.jsx":
/*!**********************************!*\
  !*** ./components/Container.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);

const Container = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "Container",
  componentId: "sc-11xeamp-0"
})(["max-width:1335px;margin:0 auto;width:100%;box-sizing:border-box;padding:20px;"]);
/* harmony default export */ __webpack_exports__["default"] = (Container);

/***/ }),

/***/ "./components/ListItem.jsx":
/*!*********************************!*\
  !*** ./components/ListItem.jsx ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mobx-react */ "mobx-react");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mobx_react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../routes */ "./routes.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_routes__WEBPACK_IMPORTED_MODULE_4__);
var _class;

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






let ListItem = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(_class = class ListItem extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    const {
      albumId,
      title,
      author
    } = this.props;
    return __jsx(_routes__WEBPACK_IMPORTED_MODULE_4__["Link"], {
      route: `/details/${albumId}`
    }, __jsx(ItemWrapper, null, __jsx(AlbumCoverWrapper, null, __jsx(AlbumCover, {
      src: `https://picsum.photos/id/${albumId}/150/150`
    })), __jsx(AlbumDetails, null, __jsx(AlbumTitle, null, title), __jsx(AlbumAuthor, null, author)), __jsx(SeeDetails, {
      className: "right"
    })));
  }

}) || _class;

const ItemWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "ListItem__ItemWrapper",
  componentId: "y7w5eg-0"
})(["padding:10px 15px;display:flex;box-sizing:border-box;align-items:center;cursor:pointer;flex-basis:33%;@media(max-width:991px){flex-basis:50%;}@media(max-width:700px){flex-basis:100%;padding:10px 0;}"]);
const AlbumCoverWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "ListItem__AlbumCoverWrapper",
  componentId: "y7w5eg-1"
})(["margin-right:15px;"]);
const AlbumCover = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.img.withConfig({
  displayName: "ListItem__AlbumCover",
  componentId: "y7w5eg-2"
})(["width:60px;border-radius:5px;"]);
const AlbumDetails = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "ListItem__AlbumDetails",
  componentId: "y7w5eg-3"
})(["display:flex;height:100%;flex-direction:column;margin-right:10px;"]);
const AlbumTitle = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.h1.withConfig({
  displayName: "ListItem__AlbumTitle",
  componentId: "y7w5eg-4"
})(["font-size:16px;font-weight:600;color:", ";margin-bottom:5px;&::first-letter{text-transform:uppercase;}"], props => props.theme.colors.black);
const AlbumAuthor = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.h4.withConfig({
  displayName: "ListItem__AlbumAuthor",
  componentId: "y7w5eg-5"
})(["color:", ";font-size:14px;"], props => props.theme.colors.grey);
const SeeDetails = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.i.withConfig({
  displayName: "ListItem__SeeDetails",
  componentId: "y7w5eg-6"
})(["border:", ";border-width:0 3px 3px 0;display:flex;padding:3px;-webkit-transform:rotate(-45deg);-ms-transform:rotate(-45deg);transform:rotate(-45deg);-webkit-transform:rotate(-45deg);height:5px;width:5px;margin-left:auto;"], props => `solid ${props.theme.colors.orange}`);
const UnderLine = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "ListItem__UnderLine",
  componentId: "y7w5eg-7"
})(["background:#dadada;background-image:-webkit-linear-gradient(left,#dadada,#dadada,#ffffff);height:2px;width:100%;"]);
ListItem.propTypes = {
  author: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  title: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string,
  albumId: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.number
};
/* harmony default export */ __webpack_exports__["default"] = (ListItem);

/***/ }),

/***/ "./components/Title.js":
/*!*****************************!*\
  !*** ./components/Title.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




class Title extends react__WEBPACK_IMPORTED_MODULE_0__["PureComponent"] {
  render() {
    return __jsx(TitleWrapper, null, this.props.title);
  }

}

const TitleWrapper = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.h1.withConfig({
  displayName: "Title__TitleWrapper",
  componentId: "sc-1f6knae-0"
})(["font-weight:600;line-height:1.2;color:#ff4865;font-size:18px;position:relative;padding-bottom:10px;&::after{width:100px;height:5px;content:\"\";position:absolute !important;bottom:0;left:-15px;z-index:0;background:-webkit-radial-gradient(50% 50%,ellipse closest-side,#ff4865,rgba(255,42,112,0) 60%);background:-moz-radial-gradient(50% 50%,ellipse closest-side,#ff4865,rgba(255,42,112,0) 60%);background:-ms-radial-gradient(50% 50%,ellipse closest-side,#ff4865,rgba(255,42,112,0) 60%);background:-o-radial-gradient(50% 50%,ellipse closest-side,#ff4865,rgba(255,42,112,0) 60%);}"]);
Title.propTypes = {
  title: prop_types__WEBPACK_IMPORTED_MODULE_1___default.a.string
};
/* harmony default export */ __webpack_exports__["default"] = (Title);

/***/ }),

/***/ "./pages/AlbumList/AlbumList.jsx":
/*!***************************************!*\
  !*** ./pages/AlbumList/AlbumList.jsx ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/values */ "../node_modules/@babel/runtime-corejs2/core-js/object/values.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! prop-types */ "prop-types");
/* harmony import */ var prop_types__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(prop_types__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! mobx-react */ "mobx-react");
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(mobx_react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _components_Container__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/Container */ "./components/Container.jsx");
/* harmony import */ var _components_ListItem__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/ListItem */ "./components/ListItem.jsx");
/* harmony import */ var _components_Title__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/Title */ "./components/Title.js");


var _dec, _class, _temp;

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;







let AlbumList = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["inject"])('store'), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_4__["observer"])(_class = (_temp = class AlbumList extends react__WEBPACK_IMPORTED_MODULE_1__["Component"] {
  constructor(...args) {
    super(...args);

    this.renderAlbumList = () => {
      const {
        store: {
          authors,
          albums
        }
      } = this.props;

      const albumsArray = albums && _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default()(albums);

      if (!albumsArray || !albumsArray.length) {
        return null;
      }

      return albumsArray.map(singleAlbum => {
        const {
          id,
          title
        } = singleAlbum;
        const authorName = authors[singleAlbum.userId] && authors[singleAlbum.userId].name;
        return __jsx(_components_ListItem__WEBPACK_IMPORTED_MODULE_6__["default"], {
          key: `album-${id}`,
          title: title,
          author: authorName,
          albumId: id
        });
      });
    };

    this.handleLoad = () => this.props.store.fetchAlbums();
  }

  static async getInitialProps({
    store
  }) {
    const {
      _page
    } = store;

    if (!_page) {
      await store.fetchAlbums();
    }

    return {};
  }

  render() {
    const {
      store: {
        loading,
        pageNumber
      }
    } = this.props;
    const albumList = this.renderAlbumList();
    console.log(process);
    return __jsx(AlbumContainer, null, __jsx(_components_Title__WEBPACK_IMPORTED_MODULE_7__["default"], {
      title: "Available albums"
    }), __jsx(AlbumListWrapper, null, albumList || 'Brak albumów do wyświetlenia'), loading && 'wczytywanie', albumList && pageNumber > 0 && __jsx(LoadMoreButton, {
      onClick: this.handleLoad
    }, "Load More"));
  }

}, _temp)) || _class) || _class);
const AlbumContainer = styled_components__WEBPACK_IMPORTED_MODULE_3___default()(_components_Container__WEBPACK_IMPORTED_MODULE_5__["default"]).withConfig({
  displayName: "AlbumList__AlbumContainer",
  componentId: "sc-3i70r2-0"
})(["margin-top:30px;margin-bottom:10px;display:flex;flex-direction:column;"]);
const AlbumListWrapper = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.div.withConfig({
  displayName: "AlbumList__AlbumListWrapper",
  componentId: "sc-3i70r2-1"
})(["display:flex;flex-wrap:wrap;margin-top:10px;"]);
const LoadMoreButton = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.button.withConfig({
  displayName: "AlbumList__LoadMoreButton",
  componentId: "sc-3i70r2-2"
})(["border:1px solid ", ";color:", ";padding:10px 50px;background-color:", ";border-radius:20px;align-self:center;margin-top:15px;:hover{cursor:pointer;}"], props => props.theme.colors.orange, props => props.theme.colors.orange, props => props.theme.colors.white);
AlbumList.propTypes = {
  store: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.shape({
    loading: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.bool,
    albums: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.objectOf(prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.shape({
      title: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string,
      id: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.number
    })),
    authors: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.objectOf(prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.shape({
      name: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.string
    })),
    fetchAlbums: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.func,
    pageNumber: prop_types__WEBPACK_IMPORTED_MODULE_2___default.a.number
  })
};
/* harmony default export */ __webpack_exports__["default"] = (AlbumList);

/***/ }),

/***/ "./pages/AlbumList/index.js":
/*!**********************************!*\
  !*** ./pages/AlbumList/index.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _AlbumList__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AlbumList */ "./pages/AlbumList/AlbumList.jsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _AlbumList__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/***/ }),

/***/ "./routes.js":
/*!*******************!*\
  !*** ./routes.js ***!
  \*******************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

const routes = __webpack_require__(/*! next-routes */ "next-routes");

module.exports = routes().add('AlbumList', '/').add('AlbumDetails', '/details/:albumId').add('AuthorDetails', '/author/:authorId');

/***/ }),

/***/ 4:
/*!****************************************!*\
  !*** multi ./pages/AlbumList/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/przemyslaw/Pulpit/moje/pokoj/modal-viewer/client/pages/AlbumList/index.js */"./pages/AlbumList/index.js");


/***/ }),

/***/ "core-js/library/fn/object/values":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/values" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/values");

/***/ }),

/***/ "mobx-react":
/*!*****************************!*\
  !*** external "mobx-react" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mobx-react");

/***/ }),

/***/ "next-routes":
/*!******************************!*\
  !*** external "next-routes" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-routes");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ })

/******/ });
//# sourceMappingURL=AlbumList.js.map