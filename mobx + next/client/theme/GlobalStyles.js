import { createGlobalStyle } from 'styled-components'
import Reset from './reset'

const GlobalStyles = createGlobalStyle`
  ${Reset}

  @font-face {
    font-family: 'Maven Pro', sans-serif;
    src: url('https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900&display=swap');
    font-weight: normal;
  }

  body {
    font-family: 'Maven Pro', sans-serif;
    color: rgba(0, 0, 0, 0.87);
  }
`

export default GlobalStyles
