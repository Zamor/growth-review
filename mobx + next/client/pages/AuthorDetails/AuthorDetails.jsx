import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'

import Container from '../../components/Container'
import ListItem from '../../components/ListItem'
import Title from '../../components/Title'

@inject('store')
@observer
class AuthorDetails extends Component {
  static async getInitialProps ({ query, store }) {
    const { authorId } = query
    await store.changeCurrentAuthor(authorId)
    return { }
  }

  renderAlbumList = () => {
    const { store: { currentAuthor: { albums } } } = this.props

    if (!albums || !albums.length) {
      return null
    }

    return albums.map(singleAlbum => {
      const { id, title } = singleAlbum

      return (
        <ListItem
          key={`album-${id}`}
          title={title}
          albumId={id}
        />
      )
    })
  }

  render () {
    const { store: { currentAuthor } } = this.props

    console.log(this.props)

    if (!currentAuthor) {
      return ' Nie istnieje taki autor'
    }

    const { name, address: { zipcode, city, street, suite }, phone, email, website, company } = currentAuthor
    return (
      <Wrapper>
        <AuthorContentDetails>
          <PersonalInformation>
            <Col>
              <SubTitle>
                {name}
              </SubTitle>
              <SingleDetail>
                Address: {zipcode} {city} {street} {suite}
              </SingleDetail>
              <SingleDetail>
                Phone: {phone || '-'}
              </SingleDetail>
              <SingleDetail>
                Email: {email || '-'}
              </SingleDetail>
              <SingleDetail>
                website: {website || '-'}
              </SingleDetail>
            </Col>
            <Col>
              <SubTitle>
                Company details
              </SubTitle>
              <SingleDetail>
                Company name: {company.name || '-'}
              </SingleDetail>
            </Col>
          </PersonalInformation>

        </AuthorContentDetails>
        <Title title='Author albums' />
        <AuthorAlbums>
          {this.renderAlbumList()}
        </AuthorAlbums>
      </Wrapper>
    )
  }
}

const Wrapper = styled(Container)`
  margin-top: 30px;
  display: flex;
  flex-direction: column;
`
const AuthorContentDetails = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 15px;

  @media(max-width: 991px){
    flex-direction: row-reverse;
    flex-wrap: wrap-reverse;
  }
`
const PersonalInformation = styled.div`
  flex-basis: 100%;
  display: flex;
  flex-wrap: wrap;

  @media(max-width: 991px){
    flex-basis: 100%;
  }
`

const Col = styled.div`
  flex-basis: 50%;

  @media(max-width: 700px) {
    flex-basis: 100%;
  }
`
const SubTitle = styled.p`
  font-weight: bold;
  margin: 10px 0;
  color: ${props => props.theme.colors.black};
`
const SingleDetail = styled.p`
  margin-bottom: 5px;
  color: ${props => props.theme.colors.grey};
`
const AuthorAlbums = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
`

AuthorDetails.propTypes = {
  store: PropTypes.shape({
    currentAuthor: PropTypes.shape({
      name: PropTypes.string,
      albums: PropTypes.array,
      address: PropTypes.shape({
        zipcode: PropTypes.string,
        city: PropTypes.string,
        street: PropTypes.string,
        suite: PropTypes.string
      }),
      phone: PropTypes.string,
      email: PropTypes.string,
      website: PropTypes.string,
      company: PropTypes.shape({
        name: PropTypes.string
      })
    })
  })
}

export default AuthorDetails
