import React from 'react'
import App from 'next/app'
import { Provider } from 'mobx-react'
import { ThemeProvider } from 'styled-components'

import themeConfig from '../theme/themeConfig'
import GlobalStyles from '../theme/GlobalStyles'

import Header from '../components/Header'
import Footer from '../components/Footer'

import initStore from '../helpers/initStore'

class DC extends App {
  static async getInitialProps ({ Component, ctx }) {
    const store = initStore()

    let componentProps = {}
    if (typeof Component.getInitialProps === 'function') {
      try {
        componentProps = await Component.getInitialProps({ store, ...ctx }) || {}
      } catch (e) {
        console.error(e) // eslint-disable-line
      }
    }

    return {
      store,
      ...componentProps
    }
  }

  constructor (props) {
    super(props)
    this.store = initStore(props.store)
  }

  render () {
    const { Component, pageProps } = this.props

    return (
      <Provider store={this.store}>
        <ThemeProvider theme={themeConfig}>
          <GlobalStyles />
          <Header />
          <Component {...pageProps} />
        </ThemeProvider>
      </Provider>
    )
  }
}

export default DC
