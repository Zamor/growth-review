const axios = require('axios')

const requestInstance = axios.create({
  baseURL: process.env.API_HOST
})

const API = {
  async request (requestType, requestParams) {
    const response = await requestInstance({
      method: requestType,
      ...requestParams
    })

    const { data } = response

    if (data) {
      return data
    }

    throw Error(data.info ? `${data.msg}: ${data.info}` : data.msg)
  },
  get (url, params = {}) {
    return this.request('get', { url, ...params })
  },
  post (url, params = {}) {
    return this.request('post', { url, ...params })
  }
}

export default API
