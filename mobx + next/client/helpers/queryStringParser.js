import { isArray } from 'lodash'

export const parseObjectToQueryString = (paramsObject = {}) => {
  const params = []
  Object.entries(paramsObject).forEach(entry => {
    const [property, value] = entry

    if (isArray(value)) {
      params.push(parseArrayToQueryString(value, property))
    } else {
      params.push(`${encodeURIComponent(property)}=${encodeURIComponent(value)}`)
    }
  })

  return params.filter(elem => elem).join('&')
}

export const parseArrayToQueryString = (data, key) => {
  const params = []
  data.forEach(elem => {
    params.push(`${encodeURIComponent(`${key}[]`)}=${elem}`)
  })

  return params.join('&')
}
