import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { observer } from 'mobx-react'

@observer
class Thumbnail extends Component {
  handleClick =() => {
    const { thumbIndex, selectThumb } = this.props
    selectThumb(thumbIndex)
  }

  render () {
    return (
      <ThumbWrapper onClick={this.handleClick}>
        <Thumb src={this.props.src} />
      </ThumbWrapper>

    )
  }
}

const ThumbWrapper = styled.div`
  padding: 10px;
  display: flex;
  flex-basis: 12.5%;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;
`
const Thumb = styled.img`
  :hover {
    cursor: pointer;
    -webkit-box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.28);
    -moz-box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.28);
    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.28);
  }
`

Thumbnail.propTypes = {
  src: PropTypes.string,
  selectThumb: PropTypes.func,
  thumbIndex: PropTypes.number
}

export default Thumbnail
