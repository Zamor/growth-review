import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { observer } from 'mobx-react'

import { Link } from '../routes'

@observer
class ListItem extends Component {
  render () {
    const { albumId, title, author } = this.props

    return (

      <Link route={`/details/${albumId}`}>
        <ItemWrapper>
          <AlbumCoverWrapper>
            <AlbumCover src={`https://picsum.photos/id/${albumId}/150/150`} />
          </AlbumCoverWrapper>
          <AlbumDetails>
            <AlbumTitle>{title}</AlbumTitle>
            <AlbumAuthor>{author}</AlbumAuthor>
          </AlbumDetails>
          <SeeDetails className='right' />
        </ItemWrapper>
      </Link>
    )
  }
}

const ItemWrapper = styled.div`
  padding: 10px 15px;
  display: flex;
  box-sizing: border-box;
  align-items: center;
  cursor: pointer;

  flex-basis: 33%;

  @media(max-width: 991px) {
    flex-basis: 50%;
  }

  @media(max-width: 700px) {
    flex-basis: 100%;
    padding: 10px 0;
  }
`
const AlbumCoverWrapper = styled.div`
  margin-right: 15px;
`
const AlbumCover = styled.img`
  width: 60px;
  border-radius: 5px;
`
const AlbumDetails = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  margin-right: 10px;
`
const AlbumTitle = styled.h1`
  font-size: 16px;
  font-weight: 600;
  color: ${props => props.theme.colors.black};
  margin-bottom: 5px;

  &::first-letter {
    text-transform: uppercase;
  }
`
const AlbumAuthor = styled.h4`
  color: ${props => props.theme.colors.grey};
  font-size: 14px;
`
const SeeDetails = styled.i`
  border: ${props => `solid ${props.theme.colors.orange}`};
  border-width: 0 3px 3px 0;
  display: flex;
  padding: 3px;
  -webkit-transform: rotate(-45deg); 
  -ms-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transform: rotate(-45deg);
  height: 5px;
  width: 5px;
  margin-left: auto;
`
const UnderLine = styled.div`
  background: #dadada;
  background-image: -webkit-linear-gradient(left, #dadada, #dadada, #ffffff);
  height: 2px;
  width: 100%;
`

ListItem.propTypes = {
  author: PropTypes.string,
  title: PropTypes.string,
  albumId: PropTypes.number
}

export default ListItem
