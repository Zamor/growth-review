import { Injectable } from '@nestjs/common';
import { Task, TaskStatus } from './task.model';
import * as uuid from 'uuid/v1'
import { CreateTaskDto } from './dto/create-task.dto';
@Injectable()
export class TasksService {
  private tasks: Task[] = [];

  getAllTasks(): Task[] {
    return this.tasks
  }

  getTaskById(id: string): Task {
    return this.tasks.find(singleTask => singleTask.id === id)
  }

  createTask(createTaskDto: CreateTaskDto): Task {
    const { title, description } = createTaskDto
    const task: Task = {
      title,
      description,
      status: TaskStatus.OPEN,
      id: uuid()
    }

    this.tasks.push(task)

    return task
  }

  deleteTask(id: string): void {
    this.tasks = this.tasks.filter(singleTask => singleTask.id !== id)
  }

  updateTaskStatus(taskId: string, newStatus: TaskStatus): Task {
    const taskObject = this.getTaskById(taskId)
    taskObject.status = newStatus

    return taskObject
  }
}
