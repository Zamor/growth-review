# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://git.streamadly.dev///compare/v1.1.0...v1.2.0) (2020-05-09)


### Features

* **project:** added storybook ([d8adb64](https://git.streamadly.dev///commit/d8adb64937b4ffea280226e55765a42fb2fe887e))

## 1.1.0 (2020-05-09)


### Features

* **gitlab:** add ci.skip option to ci ([742eac8](https://git.streamadly.dev///commit/742eac8f4e1c412af72d6d3a6d8fa8569b5b4bd0))
