import React from 'react'

import { number, text } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Heading, HeadingLevels } from '../client/components/Heading'

storiesOf('Components', module)
  .add('Headings', () => (
    <Heading
      children={text('Content', 'Initial content')}
      level={number('Heading level', 1, {
        min: 1,
        max: 6,
        step: 1
      }) as HeadingLevels}
    />
  ))
