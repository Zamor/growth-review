import React from 'react'

import { select } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { BulletList } from '../client/components/BulletList/BulletList'
import { Type } from '../client/components/BulletList/constants/type'

storiesOf('Components', module)
  .add('BulletList', () => (
    <BulletList
      type={select('Display type', {
        Bullet: Type.BULLET,
        Number: Type.NUMBER,
        Unstyled: Type.UNSTYLED
      }
      , Type.BULLET
      )}
    >
      <li>first</li>
      <li>second</li>
      <li>third</li>
    </BulletList>
  ))
  .add('BulletList - nested', () => (
    <BulletList type={Type.UNSTYLED}>
      <li>first</li>
      <li>second</li>
      <li>third
        <BulletList
          type={select('Display type', {
            Bullet: Type.BULLET,
            Number: Type.NUMBER,
            Unstyled: Type.UNSTYLED
          }
          , Type.BULLET
          )}
        >
          <li>first</li>
          <li>
            tekst
            <BulletList
              type={Type.NUMBER}
            >
              <li>first</li>
              <li>second</li>
              <li>third</li>
            </BulletList>
          </li>
          <li>third</li>
        </BulletList>
      </li>
    </BulletList>
  ))
