import React from 'react'

import { select, text, boolean } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Alert } from '../client/components/Alert/Alert'
import { Type } from '../client/components/Alert/constants/type'

storiesOf('Components', module)
  .add('Alert', () => (
    <Alert
      message={text('Message', 'Initial message')}
      closable={boolean('Closable', false)}
      type={select('Type', {
        success: Type.SUCCESS,
        error: Type.ERROR,
        warning: Type.WARNING,
        info: Type.INFO
      }
      , Type.INFO
      )}
    />
  ))
