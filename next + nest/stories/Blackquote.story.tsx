import React from 'react'

import { number, select, text } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Blockquote } from '../client/components/Blackquote/Blockquote'
import { Size } from '../client/components/Blackquote/constants/size'

storiesOf('Components', module)
  .add('Blockquote', () => (
    <Blockquote
      mb={number('Margin bottom', 15)}
      size={select('Size', {
        normal: Size.NORMAL,
        large: Size.LARGE
      }
      , Size.NORMAL
      )}
    >
      {text('Blockquote value', 'initial value')}
    </Blockquote>
  ))
