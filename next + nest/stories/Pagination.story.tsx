import React from 'react'

import { number } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Pagination } from '../client/components/Pagination/Pagination'

storiesOf('Components', module)
  .add('Pagination', () => {
    const props = {
      currentPageNumber: number('Initial page number', 1),
      pagesCount: number('Total page count', 10),
      onPageChange: (pageNumber: number) => { console.log('new page number', pageNumber) },
      backwardLabel: 'Previous',
      forwardLabel: 'Next'
    }
    return <Pagination {...props} />
  })
