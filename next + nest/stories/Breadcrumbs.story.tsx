import React from 'react'

import { storiesOf } from '@storybook/react'

import { Breadcrumbs } from '../client/components/Breadcrumbs'

storiesOf('Components', module)
  .add('Breadcrumbs', () => (
    <Breadcrumbs
      path={[
        {
          label: 'Home',
          url: 'google.pl'
        },
        {
          label: 'node 1',
          url: 'google.pl'
        }
      ]}
    />
  ))
