import React from 'react'

import { number, select, color, boolean } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { ProgressBar } from '../client/components/ProgressBar/ProgressBar'
import { Size } from '../client/components/ProgressBar/constants/size'

storiesOf('Components', module)
  .add('Progress bar', () => (
    <ProgressBar
      percentageProgress={number('Value', 40, {
        min: 0,
        max: 100,
        step: 1
      })}
      size={select('Size', {
        lg: Size.LG,
        md: Size.MD,
        sm: Size.SM,
        xs: Size.XS
      }
      , Size.LG
      )}
      bgColor={color('Bar color', '#007bff')}
      striped={boolean('Striped bar', false)}
    />
  ))
