import React from 'react'

import { select, boolean } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Button } from '../client/components/Button/Button'
import { Color } from '../client/components/Button/constants/color'
import { Size } from '../client/components/Button/constants/size'

storiesOf('Components', module)
  .add('Button', () => (
    <Button
      color={select('Color', {
        primary: Color.PRIMARY,
        secondary: Color.SECONDARY,
        success: Color.SUCCESS,
        danger: Color.DANGER,
        warning: Color.WARNING,
        info: Color.INFO
      }
      , Color.PRIMARY
      )}
      size={select('Size', {
        small: Size.SMALL,
        normal: Size.NORMAL,
        large: Size.LARGE
      }
      , Size.NORMAL
      )}
      rounded={boolean('Rounded', false)}
      outline={boolean('Outline', false)}
      isLoading={boolean('Loading', false)}
      disabled={boolean('Disabled', false)}
    >
      <div>Button Content</div>
    </Button>
  ))
