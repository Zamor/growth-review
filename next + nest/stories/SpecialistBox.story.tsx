import React from 'react'

import { storiesOf } from '@storybook/react'

import { SpecialistBox } from '../client/components/Pages/Home/SpecialistBox'

storiesOf('Pages Components - Home', module)
  .add('SpecialistBox', () => (
    <SpecialistBox />
  ))
