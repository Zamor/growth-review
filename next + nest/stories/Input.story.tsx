import React from 'react'

import { storiesOf } from '@storybook/react'

import { Input } from '../client/components/Input'

storiesOf('Components', module)
  .add('Input', () => (
    <Input
      onChange={(value: string) => console.log(value)}
    />
  ))
