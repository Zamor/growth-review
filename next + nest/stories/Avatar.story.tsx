import React from 'react'

import { select } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Avatar } from '../client/components/Avatar/Avatar'
import { AvatarGroup } from '../client/components/Avatar/AvatarGroup'
import { Size } from '../client/components/Avatar/constants/size'
import { Shape } from '../client/components/Avatar/constants/shape'
import { Status } from '../client/components/Avatar/constants/status'

storiesOf('Components', module)
  .add('Avatar', () => (
    <Avatar
      image='https://lh3.googleusercontent.com/IeNJWoKYx1waOhfWF6TiuSiWBLfqLb18lmZYXSgsH1fvb8v1IYiZr5aYWe0Gxu-pVZX3'
      size={select('Disply size', {
        XXL: Size.XXL,
        XL: Size.XL,
        l: Size.L,
        s: Size.S
      }
      , Size.L
      )}
      status={select('Status', {
        None: null,
        succes: Status.SUCCESS,
        error: Status.ERROR,
        primary: Status.PRIMARY,
        secondary: Status.SECONDARY,
        warning: Status.WARNING
      }
      , null
      )}
      shape={select('Shape', {
        circle: Shape.CIRCLE,
        rounded: Shape.ROUNDED
      }
      , Shape.CIRCLE
      )}
    />
  ))
  .add('Avatar group', () => (
    <AvatarGroup>
      <Avatar
        image='https://lh3.googleusercontent.com/IeNJWoKYx1waOhfWF6TiuSiWBLfqLb18lmZYXSgsH1fvb8v1IYiZr5aYWe0Gxu-pVZX3'
        size={Size.L}
        shape={Shape.CIRCLE}
        status={Status.WARNING}
      />
      <Avatar
        image='https://lh3.googleusercontent.com/IeNJWoKYx1waOhfWF6TiuSiWBLfqLb18lmZYXSgsH1fvb8v1IYiZr5aYWe0Gxu-pVZX3'
        size={Size.L}
        shape={Shape.CIRCLE}
        status={Status.WARNING}
      />
    </AvatarGroup>
  ))
