import React from 'react'

import { storiesOf } from '@storybook/react'

import { Highlight } from '../client/components/Typography'

storiesOf('Components', module)
  .add('Highlight', () => (
    <p>Lorem ipsum, <Highlight>quia dolor sit</Highlight></p>
  ))
