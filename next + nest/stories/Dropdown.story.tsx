import React from 'react'

import { storiesOf } from '@storybook/react'

import { Dropdown } from '../client/components/Dropdown'

storiesOf('Components', module)
  .add('Dropdown', () => (
    <div>
      <Dropdown
        items={[
          { label: 'Link', url: 'https://www.facebook.com/' },
          { label: 'Another link', url: 'https://www.naszaklasa.com/' },
          { label: 'Separated link', url: 'https://www.instagram.com/', addDivider: true }
        ]}
      >
        Dropdown
      </Dropdown>
    </div>
  ))
