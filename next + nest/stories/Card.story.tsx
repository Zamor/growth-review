import React from 'react'

import { text } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Card } from '../client/components/Card'

storiesOf('Components', module)
  .add('Card', () => (
    <Card
      imageUrl={text('Image Url', '')}
      header={text('Header', 'Header')}
      footer={text('Footer', 'Footer')}
    >
      Treść treściowa
    </Card>
  ))
