import React from 'react'

import { select } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'

import { Tabs } from '../client/components/Tabs/Tabs'
import { TabPane } from '../client/components/Tabs/TabPane'
import { Type } from '../client/components/Tabs/constants/type'
import { Align } from '../client/components/Tabs/constants/align'

storiesOf('Components', module)
  .add('Tabs', () => (
    <Tabs
      align={select('Align type', {
        Start: Align.START,
        End: Align.END,
        Justify: Align.JUSTIFY
      }
      , Align.START
      )}
      type={select('Display type', {
        Basic: Type.BASIC,
        TopLine: Type.TOPLINE,
        BottomLine: Type.BOTTOMLINE,
        Solid: Type.SOLID,
        SolidRounded: Type.SOLIDROUNDED
      }
      , Type.BASIC
      )}
      initialTab='5'
    >
      <TabPane key='12' name='1'>1</TabPane>
      <TabPane key='5' name='2'>2</TabPane>
      <TabPane key='14' name='3'>3</TabPane>
      <span key='18' name='666'><div>row1</div><div>row2</div></span>
    </Tabs>
  ))
