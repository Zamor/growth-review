module.exports = {
  verbose: true,
  testMatch: ['<rootDir>/client/**/*.(spec|test).js'],
  modulePaths: ['<rootDir>/client']
}
