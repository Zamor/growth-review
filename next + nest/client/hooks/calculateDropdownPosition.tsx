import { useRef, useState, useCallback } from 'react'
import { FlattenSimpleInterpolation, css } from 'styled-components'

type CssResult = FlattenSimpleInterpolation | null

enum HorizontalSide {
  LEFT = 'left',
  RIGHT = 'right'
}

export const calculatePositionHook = (): [(node: HTMLDivElement) => void, CssResult] => {
  const ref = useRef<HTMLDivElement | null>(null)
  const [calculatedCSS, setCalculatedCSS] = useState<CssResult>(null)

  const setRef = useCallback((node: HTMLDivElement) => {
    if (node) {
      const parentNodePos = node?.parentElement?.getBoundingClientRect()
      const dropdownNodePos = node.getBoundingClientRect()

      let horizontal: HorizontalSide = HorizontalSide.LEFT

      if ((parentNodePos?.x || 0) + dropdownNodePos.width > window.innerWidth - 10) {
        horizontal = HorizontalSide.RIGHT
      }

      const generatedCss: FlattenSimpleInterpolation = css`
        ${horizontal}: 0;
        visibility: visible;
      `

      setCalculatedCSS(generatedCss)
    } else {
      setCalculatedCSS(null)
    }

    ref.current = node
  }, [])

  return [setRef, calculatedCSS]
}
