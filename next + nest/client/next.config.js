const { BABEL_ENV } = process.env
const PRODUCTION = BABEL_ENV === 'production'

const { parsed } = require('dotenv').config()

module.exports = {
  env: parsed,
  distDir: PRODUCTION ? './../build' : './.next',
  useFileSystemPublicRoutes: false
}
