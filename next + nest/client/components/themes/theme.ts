export const theme = {
  color: {
    black: '#000',
    textDefault: '#333',
    highlight: '#fcf8e3',
    primary: '#00d0f1',
    prmiaryHover: '#0056b3',
    secondary: '#b8bdc1',
    secondaryHover: '#494f54',
    sucess: '#699834',
    sucessHover: '#19692c',
    danger: '#e84646',
    dangerHover: '#a71d2a',
    warning: '#ffbc34',
    warningHover: '#ba8b00',
    info: '#009efb',
    infoHover: '#0f6674',
    light: '#f8f9fa',
    lightHover: '#cbd3da',
    dark: '#343a40',
    darkHover: '#121416',
    muted: '#757575',
    white: '#fff',
    borderGrey: '#f0f0f0',
    borderGreyDark: '#ced4da',
    gold: '#f4c150',
    alert: {
      success: '#155724',
      error: '#721c24',
      warning: '#856404',
      info: '#004085'
    },
    button: {
      primary: '#00d0f1',
      secondary: '#6c757d',
      success: '#7bb13c',
      warning: '#ffbc34',
      danger: '#e84646',
      info: '#009efb'
    }
  }
}

type ThemeInterface = typeof theme

declare module 'styled-components' {
  interface DefaultTheme extends ThemeInterface {}
}
