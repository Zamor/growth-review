import styled from 'styled-components'

export const Input = styled.input`
  border: 1px solid ${props => props.theme.color.borderGreyDark};
  border-radius: 4px;

  font-size: 15px;

  min-height: 46px;

  padding: 6px 15px;

  width: 100%;
`
