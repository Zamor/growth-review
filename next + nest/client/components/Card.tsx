import React, { FC, ReactNode, CSSProperties } from 'react'
import styled from 'styled-components'

// -- TYPES
type CardProps = {
  imageUrl?: string
  header?: string | ReactNode
  children: string | ReactNode
  footer?: string | ReactNode
}

// -- COMPONENT
export const Card: FC<CardProps> = ({
  imageUrl,
  header,
  children,
  footer
}: CardProps) => (
  <Wrapper>
    {imageUrl && (
      <HeaderImage src={imageUrl} alt='Card image' />
    )}
    {header && (
      <Header>
        {header}
      </Header>
    )}
    <Content>
      {children}
    </Content>
    {footer && (
      <Footer>
        {footer}
      </Footer>
    )}
  </Wrapper>
)

// -- STYLED
const Wrapper = styled.div`
  display: flex;

  flex-direction: column;

  border: 1px solid ${props => props.theme.color.borderGrey};
  border-radius: 4px;

  margin-bottom: 30px;

  background-color: ${props => props.theme.color.white};

  width: 100%;
`
const HeaderImage = styled.img`
  width: 100%;
`
const Header = styled.h5`
  padding: 16px 24px;
  margin: 0;

  border-bottom: 1px solid ${props => props.theme.color.borderGrey};

  font-size: 20px;
  font-family: 'poppinsmedium';
`
const Content = styled.div`
  padding: 24px;
`
const Footer = styled.div`
  border-top: 1px solid ${props => props.theme.color.borderGrey};

  padding: 16px 24px;

  color: ${props => props.theme.color.muted};
`
