export enum Align {
  START = 'start',
  END = 'end',
  JUSTIFY = 'justify'
}
