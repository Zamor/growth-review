export enum Type {
  BASIC = 'basic',
  TOPLINE = 'top-line',
  BOTTOMLINE = 'bottom-line',
  SOLID = 'solid',
  SOLIDROUNDED = 'solid-rounded'
}
