import styled from 'styled-components'

export const MenuItemBase = styled.li<{ active?: boolean }>`
  text-align: center;
  color: #888;

  background-color: 'rgba(0, 0, 0, 0)';

  transition: background-color 0.4s ease;

  cursor: pointer;

  &:hover {
    color: #495057;

    background-color: #eee;
  }

  ${props => props.active && `
    color: #495057;
  `}

  box-sizing: border-box;

  border-radius: 0;
  border: 1px solid transparent;
`
export const MenuItemBasic = styled(MenuItemBase)`
  margin-bottom: -1px;

  ${props => props.active && `
    border-width: 1px 1px 0px 1px;
    border-style: solid;
    border-color: #e6e6e6;

    background-color: #fff;
  `}
`
export const MenuItemTopLine = styled(MenuItemBase)`
  a {
    border-top: 2px solid rgba(0, 0, 0, 0);

    ${props => props.active && `
      border-top-color: #00d0f1;
    `}

    ${props => !props.active && `
      &:hover {
        border-top-color: #ddd;
      }
    `}
  }
`
export const MenuItemBottomLine = styled(MenuItemBase)`
  margin-bottom: -1px;

  a {
    border-bottom: 2px solid rgba(0, 0, 0, 0);

    ${props => props.active && `
      border-bottom-color: ${props.theme.color.primary};
    `}

    ${props => !props.active && `
      &:hover {
        border-bottom-color: #ddd;
      }
    `}
  }
`
export const MenuItemSolid = styled(MenuItemBase)`
  color: #495057;

  &:hover {
    ${props => props.active && `
      background-color: ${props.theme.color.primary};
      color: ${props.theme.color.white};
    `}

    ${props => !props.active && `
      background-color: #f5f5f5;
    `}
  }

  ${props => props.active && `
    background-color: ${props.theme.color.primary};
    color: ${props.theme.color.white};
  `}
`
export const MenuItemSolidRounded = styled(MenuItemSolid)`
  border-radius: 50px;
`
