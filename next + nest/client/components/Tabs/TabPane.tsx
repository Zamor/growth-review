import { FC } from 'react'

// -- TYPES
export type TabPaneProps = {
  key: string | number
  name: string
  children: any
}

// -- COMPONENT
export const TabPane: FC<TabPaneProps> = ({
  children
}: TabPaneProps) => children
