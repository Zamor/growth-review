import React, { FC, ReactNode } from 'react'
import styled from 'styled-components'

import { Type } from './constants/type'
import { MenuItemBasic, MenuItemTopLine, MenuItemBottomLine, MenuItemSolid, MenuItemSolidRounded } from './TabTypes'

// -- TYPES
type TabMenuItemProps = {
  type: Type,
  active?: boolean,
  children: ReactNode,
  onClick: () => any
}

// -- HELPERS
const componentMap = {
  basic: MenuItemBasic,
  'top-line': MenuItemTopLine,
  'bottom-line': MenuItemBottomLine,
  solid: MenuItemSolid,
  'solid-rounded': MenuItemSolidRounded
}

// -- COMPONENT
export const TabMenuItem: FC<TabMenuItemProps> = ({
  children,
  type,
  active,
  onClick
}: TabMenuItemProps) => {
  const Component = componentMap[type]

  return (
    <Component active={active} onClick={onClick}>
      <A>{children}</A>
    </Component>
  )
}

// -- STYLED
export const A = styled.a`
  padding: 8px 16px;
  text-decoration: none;
  display: block;
`
