import React, { FC, useEffect, useState, ReactNode, ReactNodeArray } from 'react'
import styled from 'styled-components'

import { TabPaneProps } from './TabPane'

import { TabMenuItem } from './TabMenuItem'

import { Type } from './constants/type'
import { Align } from './constants/align'

// -- TYPES
type TabsProps = {
  type?: Type
  align?: Align
  initialTab?: string
  children: React.ReactElement<TabPaneProps>[] | React.ReactElement<TabPaneProps>
}

// -- COMPONENT
export const Tabs: FC<TabsProps> = ({
  type = Type.BASIC,
  align = Align.START,
  initialTab,
  children
}: TabsProps) => {
  const [activeTab, setActiveTab] = useState<string | number | null>(null)

  useEffect(() => {
    if (!Array.isArray(children)) {
      return
    }

    const selectedTab = children.find((singleChild) => singleChild.props.name && singleChild.key === initialTab)
    if (selectedTab) {
      return setActiveTab(selectedTab.key)
    }

    const firstAvailable = children.filter((singleChild) => singleChild.key && singleChild.props.name)[0]
    setActiveTab(firstAvailable ? firstAvailable.key : null)
  }, [children, initialTab])

  const renderMenuItems = (): ReactNodeArray => {
    const childrenToRender = Array.isArray(children) ? children : [children]

    return childrenToRender.map(({ key, props: { name } }) =>
      key && name && (
        <TabMenuItem
          key={key}
          type={type}
          active={key === activeTab}
          onClick={onTabChange(key)}
        >
          {name}
        </TabMenuItem>
      )
    )
  }

  const renderTabContent = (): ReactNode | null => {
    const childrenToRender = Array.isArray(children) ? children : [children]
    const tabContent = childrenToRender.find(singleChild => singleChild.key === activeTab)

    return tabContent
  }

  const onTabChange = (tabKey: string | number) => () => setActiveTab(tabKey)

  if (!activeTab) {
    return null
  }

  return (
    <div>
      <TabMenu align={align} type={type}>
        {renderMenuItems()}
      </TabMenu>
      {renderTabContent()}
    </div>
  )
}

// -- STYLED
const TabMenu = styled.ul<{ align: Align, type: Type }>`
  display: flex;

  list-style: none;

  margin: 0 0 20px 0;
  padding: 0;

  border-bottom: 1px solid #e6e6e6;

  
  ${props => ['start', 'end'].includes(props.align) && `
    justify-content: flex-${props.align};
  `}

  ${props => !['start', 'end'].includes(props.align) && `
    justify-content: space-evenly;

    li {
      width: 100%;
    }
  `}

  ${props => ['solid', 'solid-rounded'].includes(props.type) && `
    background-color: #fafafa;
    border-bottom: none;
  `}
`
