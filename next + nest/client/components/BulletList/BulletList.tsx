import styled from 'styled-components'

import { Type } from './constants/type'

export const BulletList = styled.ul<{ type: Type }>`
  padding-left: 40px;

  color: ${props => props.theme.color.textDefault};

  line-height: 1.5;

  ${props => props.type === Type.UNSTYLED && 'list-style: none;'}
  ${props => props.type === Type.NUMBER && 'list-style-type: decimal;'}
  ${props => props.type === Type.BULLET && 'list-style-type: disc;'}
`
