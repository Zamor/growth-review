export enum Type {
  BULLET = 'bullet',
  NUMBER = 'number',
  UNSTYLED = 'unstyled'
}
