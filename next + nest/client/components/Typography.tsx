import styled from 'styled-components'

// -- STYLED
export const Highlight = styled.mark`
  padding: 3px;

  background-color: ${props => props.theme.color.highlight}
`

export const Del = styled.del`
  text-decoration: line-through;
`

export const S = styled.s`
  text-decoration: line-through;
`

export const Additional = styled.ins`
  text-decoration: underline;
`

export const Underline = styled.u`
  text-decoration: underline;
`

export const Small = styled.small`
  font-size: 80%;
`

export const Bold = styled.b`
  font-weight: bolder;
`

export const Italic = styled.em`
  font-style: italic;
`

export const Monospace = styled.p`
  font-family: SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace!important;
  margin-bottom: 0;
`

export const Paragraph = styled.p`
  margin-bottom: 16px;
`

export const Link = styled.a`
  text-decoration: none;
  background-color: transparent;
  cursor: pointer;
`
