import React, { FC, ReactNode } from 'react'
import styled from 'styled-components'

import { Size } from './constants/size'

// -- TYPES
type PaginationProps = {
  currentPageNumber: number
  pagesCount: number
  onPageChange: (pageNumber: number) => void
  forwardLabel?: string
  backwardLabel?: string,
  size?: Size
}

// -- COMPONENT
export const Pagination: FC<PaginationProps> = ({
  currentPageNumber,
  pagesCount,
  onPageChange,
  forwardLabel = '»',
  backwardLabel = '«',
  size = Size.MEDIUM
}: PaginationProps) => {
  const renderControlItems = (): ReactNode => {
    const currentPage = currentPageNumber > pagesCount ? pagesCount : currentPageNumber
    const paginationItems = [backwardLabel, ...getAvailablePages(), forwardLabel]

    return paginationItems.map((singleItem, index) => {
      const disabled =
        (index === 0 && currentPage === 1) || (index === paginationItems.length - 1 && currentPage === pagesCount)
      const active = Number(singleItem) === (currentPageNumber > pagesCount ? pagesCount : currentPageNumber)

      return (
        <PaginationItem
          key={`page-${singleItem}`}
          active={active}
          disabled={disabled}
          size={size}
          onClick={!active && !disabled ? handlePageChange(singleItem) : undefined}
        >
          {singleItem}
        </PaginationItem>
      )
    })
  }

  const getAvailablePages = (): number[] => {
    const currentPage =
      currentPageNumber > pagesCount ? pagesCount : currentPageNumber

    const isFirst = currentPage === 1
    const isLast = currentPage === pagesCount

    if (isFirst || isLast) {
      let availablePages = isFirst
        ? [1, 2, 3]
        : [pagesCount - 2, pagesCount - 1, pagesCount]

      if (pagesCount < 3) {
        availablePages = isFirst
          ? availablePages.slice(0, pagesCount)
          : availablePages.slice(3 - pagesCount)
      }

      return availablePages
    }

    return [currentPage - 1, currentPage, currentPage + 1]
  }

  const handlePageChange = (page: number | string) => (): void => {
    if (page === backwardLabel) {
      return onPageChange(currentPageNumber - 1)
    }

    if (page === forwardLabel) {
      return onPageChange(currentPageNumber + 1)
    }

    return onPageChange(Number(page))
  }

  return (
    <PaginationWrapper>
      {renderControlItems()}
    </PaginationWrapper>
  )
}

// -- STYLES
const PaginationWrapper = styled.ul`
  display: flex;

  list-style: none;

  background-color: #fff;

  li:first-child {
    border-radius: 5px 0 0 5px;
  }

  li:last-child {
    border-radius: 0 5px 5px 0;
  }
`
const PaginationItem = styled.li<{ active?: boolean, disabled?: boolean, size?: Size }>`
  margin-left: -1px;

  border: 1px solid #dee2e6;

  color: #00d0f1;

  cursor: pointer;

  ${props => props.size === 'small' && `
    padding: 4px 8px;

    font-size: 14px;
  `}

  ${props => props.size === 'medium' && `
    padding: 8px 12px;
  `}

  ${props => props.size === 'large' && `
    padding: 12px 24px;

    font-size: 20px;
  `}

  ${props => !props.disabled && !props.active && `
    &:hover {
      background-color: #e9ecef;
    }
  `}

  ${props => !props.disabled && props.active && `
    color: #fff;

    background-color: #00d0f1;

    border-color: #00d0f1;
  `}

  ${props => props.disabled && `
    color: #6c757d;

    cursor: default;
  `}
`
