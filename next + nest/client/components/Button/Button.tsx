import React, { FC, ReactNode } from 'react'
import styled, { keyframes } from 'styled-components'

import { Color } from './constants/color'
import { Size } from './constants/size'

// -- TYPES
type ButtonProps = {
  color?: Color
  rounded?: boolean
  outline?: boolean
  disabled?: boolean
  isLoading?: boolean
  size?: Size
  children: ReactNode
}

// -- COMPONENT
export const Button: FC<ButtonProps> = ({
  color = Color.PRIMARY,
  rounded = false,
  outline = false,
  disabled = false,
  isLoading = false,
  size = Size.NORMAL,
  children,
  ...rest
}: ButtonProps) => (
  <ButtonElement
    color={color}
    rounded={rounded}
    outline={outline}
    disabled={disabled}
    size={size}
    {...rest}
  >
    {isLoading && <Loader />}
    {children}
  </ButtonElement>
)

// -- STYLED
const ButtonElement = styled.button<{
  color: Color, rounded: boolean, outline: boolean, disabled: boolean, size: Size
}>`
  display: flex;

  align-items: center;
  justify-content: center;

  padding: 6px 12px;

  color: ${props => props.theme.color.white};

  background-color: ${props => props.theme.color.button[props.color]};

  border: 1px solid ${props => props.theme.color.button[props.color]};
  border-radius: 4px;

  cursor: pointer;


  ${props => props.outline && `
    background-color: ${props.theme.color.white};

    color: ${props.theme.color.button[props.color]};

    border-color: ${props.theme.color.button[props.color]};

    &:hover {
      color: ${props.theme.color.white};

      background-color: ${props.theme.color.button[props.color]};
    }
  `}

  ${props => props.rounded && `
    border-radius: 50px;
  `}

  ${props => props.disabled && `
    background-color: #f8f9fa;

    color: #a6a6a6;

    border-color: #e6e6e6;

    cursor: not-allowed;
  `}

  ${props => props.size === Size.LARGE && `
    font-size: 20px;

    padding: 8px 16px;
  `}

  ${props => props.size === Size.SMALL && `
    font-size: 14px;

    padding: 4px 8px;
  `}
`
const Animation = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`

const Loader = styled.div`
  margin-right: 10px;

  border: 3px solid ${props => props.theme.color.white};
  border-radius: 50%;
  border-top: 3px solid rgba(0, 0, 0, 0);

  width: 10px;
  height: 10px;

  animation: ${Animation} 1s linear infinite;
`
