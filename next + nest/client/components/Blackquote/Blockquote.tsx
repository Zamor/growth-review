import styled from 'styled-components'

import { Size } from './constants/size'

export const Blockquote = styled.blockquote<{ size?: Size, mb?: number }>`
  font-size: ${props => props.size === 'large' ? 20 : 16}px;

  margin: 0 0 ${props => props.mb || 16}px 0;
`
