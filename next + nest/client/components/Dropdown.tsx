import React, { FC, ReactNode, useState, useEffect } from 'react'
import styled, { FlattenSimpleInterpolation } from 'styled-components'
import Link from 'next/link'

import { calculatePositionHook } from '../hooks/calculateDropdownPosition'

// -- TYPES
type DropdownItemProps = {
  url: string
  label: string
  addDivider?: boolean
}

type DropdownProps = {
  children: string | ReactNode
  items: DropdownItemProps[]
}

// -- COMPONENT
export const Dropdown: FC<DropdownProps> = ({
  children,
  items
}: DropdownProps) => {
  const [show, setShowStatus] = useState<boolean>(false)
  const [dropdownRef, calculatedCSS] = calculatePositionHook()

  useEffect(() => {
    show
      ? document.body.addEventListener('click', handleClickOutside, false)
      : document.body.removeEventListener('click', handleClickOutside, false)
  }, [show])

  useEffect(() => () => document.body.removeEventListener('click', handleClickOutside, false), [])

  const handleClickOutside = (e: any): void | boolean =>
    !dropdownRef?.current?.contains(e.target) && setShowStatus(false)

  const renderItems = (): ReactNode => {
    if (!Array.isArray(items)) {
      return null
    }

    return items.map(singleMenuItem => {
      const { label, url, addDivider } = singleMenuItem

      return (
        <>
          {addDivider && <Divider key={`divider-before-${label}`} />}
          <Link href={url} key={`${url}-${label}`}>
            <MenuItem>{label}</MenuItem>
          </Link>
        </>
      )
    })
  }

  const setAsShown = () => {
    setShowStatus(!show)
  }

  return (
    <Wrapper onClick={setAsShown}>
      {children}
      {show && (
        <DropdownContent
          ref={dropdownRef}
          positionStyles={calculatedCSS}
        >
          {renderItems()}
        </DropdownContent>
      )}
    </Wrapper>
  )
}

// -- STYLED
const Wrapper = styled.a`
  position: relative;

  &::after {
    display: inline-block;

    margin-left: .255em;

    vertical-align: .255em;

    content: "";

    border-top: .3em solid;
    border-right: .3em solid transparent;
    border-bottom: 0;
    border-left: .3em solid transparent;
  }
`
const DropdownContent = styled.div<{
  positionStyles: FlattenSimpleInterpolation | null
}>`
  position: absolute;

  visibility: hidden;

  padding: 8px 0;
  margin: 2px 0;

  border: 1px solid ${props => props.theme.color.borderGrey};
  border-radius: 4px;

  min-width: 100%;

  ${props => props.positionStyles};
`
const MenuItem = styled.a`
  display: flex;

  white-space: nowrap;

  padding: 4px 24px;

  font-size: 14px;

  cursor: pointer;

  &:hover {
    background-color: #f8f9fa;
  }
`
const Divider = styled.div`
  height: 0;

  margin: 8px 0;

  overflow: hidden;

  border-top: 1px solid ${props => props.theme.color.borderGrey};
`
