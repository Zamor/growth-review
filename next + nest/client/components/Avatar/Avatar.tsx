import React, { FC } from 'react'
import styled from 'styled-components'

import { Size } from './constants/size'
import { Status } from './constants/status'
import { Shape } from './constants/shape'

// -- TYPES
interface AvatarProps {
  size?: Size,
  image: string,
  status?: Status,
  shape?: Shape
}

// -- COMPONENT
export const Avatar: FC<AvatarProps> = ({
  size = Size.L,
  image,
  status,
  shape = Shape.CIRCLE
}: AvatarProps) => (
  <Wrapper size={size} status={status}>
    <AvatarImage src={image} alt='opcjonalna fotka' shape={shape} />
  </Wrapper>
)

// -- STYLED
const Wrapper = styled.div<{ size: Size, status?: Status }>`
  width: ${props => props.size}px;
  height: ${props => props.size}px;

  position: relative;

  ${props => props.status && `
    &::before {
      position: absolute;

      right: 0;
      bottom: 0;

      width: 25%;
      height: 25%;

      border-radius: 50%;
      border: 2px solid #fff;

      content: '';

      background-color: ${props.theme.color[props.status]}
    }
  `}
`
const AvatarImage = styled.img<{ shape: Shape }>`
  width: 100%;
  height: 100%;

  border-radius: ${props => props.shape === 'circle' ? '50%' : '6px'};

  object-fit: cover;
`
