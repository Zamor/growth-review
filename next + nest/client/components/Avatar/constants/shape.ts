export enum Shape {
  CIRCLE = 'circle',
  ROUNDED = 'rounded'
}
