export enum Size {
  XXL = 128,
  XL = 80,
  L = 60,
  S = 48
}
