export enum Status {
  SUCCESS = 'success',
  ERROR = 'error',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  WARNING = 'warning'
}
