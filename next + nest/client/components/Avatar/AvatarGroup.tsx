import styled from 'styled-components'

// -- STYLED
export const AvatarGroup = styled.div`
  display: flex;

  > div {
    &:not(:first-child) {
      margin-left: -12px;
    }

    &:hover {
      z-index: 1;
    }
  }

  img {
    border: 3px solid #fff;
  }

`
