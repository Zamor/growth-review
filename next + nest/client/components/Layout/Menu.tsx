import React, { FC } from 'react'
import styled from 'styled-components'
import Link from 'next/link'

import { MenuItemProps, MenuItem } from './MenuItem'
import { Button } from '../Button/Button'
import { Size } from '../Button/constants/size'

// -- TYPES
type MenuProps = {
  menuItems: MenuItemProps[]
}

// -- COMPONENT
export const Menu: FC<MenuProps> = ({ menuItems }: MenuProps) => {
  const renderMenuItem = () => {
    if (!Array.isArray(menuItems)) {
      return null
    }

    return menuItems.map(singleMenuItem => (
      <MenuItem {...singleMenuItem} key={`${singleMenuItem.label}`} />
    ))
  }

  return (
    <Nav>
      <MenuItemsWrapper>
        {renderMenuItem()}
      </MenuItemsWrapper>
      <div>
        <Link href='/login'>
          <a>
            <Button
              outline
              size={Size.LARGE}
            >
              ZALOGUJ
            </Button>
          </a>
        </Link>
      </div>
    </Nav>
  )
}

// -- STYLED
const Nav = styled.nav`
  display: flex;

  flex-grow: 1;

  align-items: center;
  justify-content: space-between;
`
const MenuItemsWrapper = styled.ul`
  display: flex;

  list-style: none;

  padding-left: 0;
`
