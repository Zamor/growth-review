import React, { FC } from 'react'
import styled from 'styled-components'

import { CategoryElement } from './CategoryElement'

// -- TYPES
export type CategoryItemSingle = {
  label: string
  route: string
}

type CategoryItemWithChildrens = {
  label: string
  childrens: CategoryItemSingle[]
}

export type CategoryItem = (CategoryItemSingle | CategoryItemWithChildrens)

// -- MOCKS
const categoriesMock: CategoryItem[] = [
  {
    label: 'Fizjoterapeuta',
<<<<<<< HEAD
    route: '/specialist?spec=fizio'
=======
    route: '/specialist/fizio'
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70
  },
  {
    label: 'Psycholog',
    childrens: [
      {
        label: 'Uzależnienia',
<<<<<<< HEAD
        route: '/specialist?spec=addictions'
      },
      {
        label: 'Depresja',
        route: '/specialist?spec=depression'
=======
        route: '/specialist/addiction'
      },
      {
        label: 'Depresja',
        route: '/specialist/depression'
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70
      }
    ]
  },
  {
    label: 'Dermatolog',
<<<<<<< HEAD
    route: '/specialist?spec=dermatologist'
=======
    route: '/specialist/therma'
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70
  }
]

// -- COMPONENT
export const CategoriesList: FC<{ }> = () => {
  const renderCategoryItems = (categoriesArray: CategoryItem[]) => {
    if (!Array.isArray(categoriesArray)) {
      return null
    }

    return categoriesArray.map(singleCategory => <CategoryElement {...singleCategory} key={singleCategory.label} />)
  }

  return (
    <Wrapper>
      {renderCategoryItems(categoriesMock)}
    </Wrapper>
  )
}

// -- STYLED
const Wrapper = styled.ul`
  padding: 0;

  margin: 30px -24px 0;

  list-style: none;

  border-bottom: 1px solid ${props => props.theme.color.borderGreyDark};
`
