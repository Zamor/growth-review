import React, { FC, useState, ReactNode, useEffect } from 'react'
import styled from 'styled-components'
import Link from 'next/link'
import { useRouter } from 'next/router'

import { CategoryItemSingle } from './CategoriesList'

// -- TYPES
type CategoryElementProps = {
  label: string
  route?: string
  childrens?: CategoryItemSingle[]
}

// -- COMPONENT
export const CategoryElement: FC<CategoryElementProps> = ({
  label,
  route,
  childrens
}: CategoryElementProps) => {
  const router = useRouter()

  const [expandStatus, setExpandStatus] = useState<boolean>(false)

  useEffect(() => {
    const isChildrenActive = Array.isArray(childrens) &&
      childrens.find(singleChildren => singleChildren.route === router.asPath)

    if (expandStatus !== !!isChildrenActive) {
      setExpandStatus(!!isChildrenActive)
    }
  }, [childrens])

  const renderLabel = (): ReactNode => {
    if (!route) {
      return label
    }

    return (
      <Link href={route}>
        <a>
          {label}
        </a>
      </Link>
    )
  }

  const renderIndentItems = (): ReactNode => Array.isArray(childrens) && childrens.map(({ label, route }) => (
    <Item
      key={label}
      active={router.asPath === route}
    >
      <Link href={route}>
        <a>
          {label}
        </a>
      </Link>
    </Item>
  ))

  const handleExpand = (): void => {
    !route && setExpandStatus(!expandStatus)
  }

  return (
    <>
      <Item
        onClick={handleExpand}
        active={router.asPath === route}
      >
        {renderLabel()}
      </Item>
      {expandStatus && (
        <Indent>
          {renderIndentItems()}
        </Indent>
      )}
    </>
  )
}

// -- STYLED
const Item = styled.li<{ active: boolean }>`
  padding: 10px 24px;

  border-top: 1px solid ${props => props.theme.color.borderGreyDark};

  cursor: pointer;

  ${props => props.active && `
    a {
      color: ${props.theme.color.primary};
    }
  `}
`
const Indent = styled.ul`
  padding: 0;

  margin: 0;

  list-style: none;

  li {
    padding-left: 50px;
  }
`
