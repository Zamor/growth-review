import React, { FC, ChangeEvent } from 'react'

import { Card } from '../../Card'
import { Input } from '../../Input'
import { CategoriesList } from './CategoriesList'

// -- COMPONENT
export const CategorySideMenu: FC<{}> = () => {
  const onFilterChange = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
  }

  return (
    <Card
      header='Wybierz kategorię'
    >
      <Input
        onChange={onFilterChange}
        defaultValue='tezt'
      />

      <CategoriesList />
    </Card>
  )
}

// -- STYLED
