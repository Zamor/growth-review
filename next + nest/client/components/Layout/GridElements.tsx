
import styled from 'styled-components'

// -- TYPES
interface ColumnProps {
  xl?: number | string,
  lg?: number | string,
  md?: number | string,
  s?: number | string,
  es?: number | string,
}

// -- COMPONENTS
export const Row = styled.div`
  display: flex;

  flex-wrap: wrap;

  margin: 0 -15px;
`

const generateWidth = (key: keyof ColumnProps, props: ColumnProps) => {
  const { xl, lg, md, s, es } = props
  const selectedWidth = props[key] || es || s || md || lg || xl || 'auto'

  return typeof selectedWidth === 'number' ? `${selectedWidth}%` : selectedWidth
}

export const Column = styled.div<ColumnProps>`
  display: flex;

  padding: 0 15px;

  ${props => `
    @media (max-width: 576px) {
      width: ${generateWidth('es', props)};
    }

    @media (min-width: 576px) {
      width: ${generateWidth('s', props)};
    }

    @media (min-width: 768px) {
      width: ${generateWidth('md', props)};
    }


    @media (min-width: 992px) {
      width: ${generateWidth('lg', props)};
    }

    @media (min-width: 1200px) {
      width: ${generateWidth('xl', props)};
    }
  `}
`
