import React, { FC } from 'react'
import styled from 'styled-components'
import Link from 'next/link'

import { MenuItemProps } from './MenuItem'
import { Menu } from './Menu'

// TODO: do wyrzucenia
const MenuMock: MenuItemProps[] = [
  {
    label: 'E-wizyta krok po kroku',
    route: '/visit'
  },
  {
    label: 'E-recepta',
    route: '/prescription'
  },
  {
    label: 'E-zwolnienie',
    route: '/sickLeave'
  },
  {
    label: 'Praca AlfaCare.pl',
    route: '/workWithUs'
  }
]

// -- COMPONENT
export const TopBar: FC<{}> = () => (
  <Wrapper>
    <Link href='/'>
      <a>
        <Logo
<<<<<<< HEAD
          // src='https://dreamguys.co.in/demo/doccure/template/assets/img/logo.png'
          src='/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/static/logo.png'
=======
          src='/static/logo.png'
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70
        />
      </a>
    </Link>

    <Menu
      menuItems={MenuMock}
    />
  </Wrapper>
)

// -- STYLED
const Wrapper = styled.header`
  position: fixed;

  z-index: 5;

  width: 100%;

  height: 85px;

  border-bottom: 1px solid ${props => props.theme.color.borderGrey};

  background-color: ${props => props.theme.color.white};

  padding: 0 30px;

  display: flex;

  align-items: center;
`
const Logo = styled.img`
  width: 160px;

  margin-right: 30px;
`
