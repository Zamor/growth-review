import React, { FC, ReactNode } from 'react'
import styled from 'styled-components'

import { TopBar } from './TopBar'

// -- TYPES
type LayoutProps = {
  children: ReactNode
}

// -- COMPONENT
export const Layout: FC<LayoutProps> = ({
  children
}: LayoutProps) => {
  const cos = ''

  return (
    <>
      <TopBar />
      <PageContent>
        {children}
      </PageContent>
    </>
  )
}

// -- STYLED
const PageContent = styled.div`
  padding: 115px 30px 0;

  background-color: ${props => props.theme.color.light};

  height: 100vh;
`
