import React, { FC } from 'react'
import styled from 'styled-components'
import Link from 'next/link'
import { useRouter } from 'next/router'

// -- TYPES
export type MenuItemProps = {
  label: string
  route: string
}

// -- COMPONENT
export const MenuItem: FC<MenuItemProps> = ({
  label,
  route
}: MenuItemProps) => {
  const router = useRouter()

  return (
    <Wrapper active={router.pathname === route}>
      <Link href={route}>
        <a>
          {label}
        </a>
      </Link>
    </Wrapper>
  )
}

// -- STYLED
const Wrapper = styled.li<{ active: boolean }>`
  margin-right: 30px;

  a {
    font-size: 14px;
<<<<<<< HEAD
    font-weight: 500;
=======
    font-family: 'poppinsmedium';
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70

    color: ${props => props.theme.color.textDefault};

    ${props => props.active && `
      color: ${props.theme.color.primary};
    `}
  }

  a:hover {
    color: ${props => props.theme.color.primary};
  }
`
