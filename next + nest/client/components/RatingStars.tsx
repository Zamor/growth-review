import React, { FC } from 'react'
import styled from 'styled-components'

import { Icon } from './Icon'
import { faStar, faStarHalf } from '@fortawesome/free-solid-svg-icons'

// -- TYPES
type RatingStarsProps = {
  rating: number
}

// -- COMPONENT
export const RatingStars: FC<RatingStarsProps> = ({ rating }: RatingStarsProps) => {
  const renderStars = (value: number) => {
    const fullStarsNumber = Math.floor(value) || 0
    const fullStars = new Array(fullStarsNumber).fill('').map((singleStar, index) => <Icon key={`star-${index}`} iconType={faStar} />)
    const halfStar = value % 1 ? <HalfStar iconType={faStarHalf} /> : null

    return (
      <> {fullStars} {halfStar} </>
    )
  }

  return (
    <Wrapper>
      <GoldenStarWrapper>
        {renderStars(rating)}
      </GoldenStarWrapper>
      <GreyStarWrapper>
        {renderStars(5 - rating)}
      </GreyStarWrapper>
    </Wrapper>
  )
}

// -- STYLED
const Wrapper = styled.div`
  display: flex;

  margin-right: 5px;
`
const GoldenStarWrapper = styled.span`
  svg {
    color: ${props => props.theme.color.gold};
  }
`
const GreyStarWrapper = styled.span`
  transform: scaleX(-1);

  margin-left: -18px;

  svg {
    color: ${props => props.theme.color.borderGreyDark};
  }
`
const HalfStar = styled(Icon)`
  margin-left: -4.5px;
`
