import React, { FC, memo } from 'react'
import styled from 'styled-components'

// -- TYPES
export type HeadingLevels = 1 | 2 | 3 | 4| 5 | 6

type HeadingProps = {
  children: string,
  level: HeadingLevels
}

type NativeHeadingProps = {
  headingLevel: HeadingLevels
  children: string
  className?: string
}

// -- CONF
const sizeConf: {[ property in HeadingLevels]: number} = {
  1: 40,
  2: 32,
  3: 28,
  4: 24,
  5: 20,
  6: 16
}

// -- COMPONENTS
export const Heading: FC<HeadingProps> = memo(({ children, level, ...rest }: HeadingProps) =>
  <H as={`h${level}`} headingLevel={level} {...rest}>{children}</H>
)

// -- STYLES
const H = styled.h1<{ headingLevel: HeadingLevels }>`
  margin: 0 0 8px 0;
  font-size: ${props => sizeConf[props.headingLevel]}px;
`
