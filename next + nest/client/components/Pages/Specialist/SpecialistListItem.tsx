import React, { FC } from 'react'
import styled from 'styled-components'

import { faMoneyBillAlt, faComment, faThumbsUp } from '@fortawesome/free-regular-svg-icons'

import { Card } from '../../Card'
import { Avatar } from '../../Avatar/Avatar'
import { Size as AvatarSize } from '../../Avatar/constants/size'
import { Shape as AvatarShape } from '../../Avatar/constants/shape'
import { Heading } from '../../Heading'
import { Paragraph } from '../../Typography'
import { Button } from '../../Button/Button'
import { Size as ButtonSize } from '../../Button/constants/size'

import { Icon } from '../../Icon'
import { RatingStars } from '../../RatingStars'

// -- TYPES
export interface Doctor {
  fullName: string
  specialisation: string
  category: string
  ratingsNumber: number
  recomendation: number
  rating: number
  pricing: string
  feedbacksNumber: number
  photo: string
}

// -- COMPONENT
export const SpecialistListItem: FC<Doctor> = ({
  fullName,
  specialisation,
  category,
  ratingsNumber,
  recomendation,
  feedbacksNumber,
  pricing,
  photo,
  rating
}: Doctor) => (
  <Card>
    <CardContentWrapper>
      <DoctorDetailsLeft>
        <ImageWrapper>
          <Avatar
            image={photo}
            shape={AvatarShape.ROUNDED}
            size={AvatarSize.XXL}
          />
        </ImageWrapper>
        <Details>
          <Heading level={5}>
            {fullName}
          </Heading>
          <Specialisation>
            {specialisation}
          </Specialisation>
          <Category level={5}>
            {category}
          </Category>
          <RatingsWrapper>
            <RatingStars rating={rating} /> {`(${ratingsNumber})`}
          </RatingsWrapper>
        </Details>
      </DoctorDetailsLeft>

      <DoctorDetailsRight>
        <DetailWrapper>
          <StyledIcon iconType={faThumbsUp} /> {recomendation}%
        </DetailWrapper>
        <DetailWrapper>
          <StyledIcon iconType={faComment} /> {feedbacksNumber} Opinii
        </DetailWrapper>
        <DetailWrapper>
          <StyledIcon iconType={faMoneyBillAlt} /> {pricing}zł
        </DetailWrapper>
        <ActionButton outline size={ButtonSize.LARGE}>
          Zobacz profil
        </ActionButton>
        <ActionButton size={ButtonSize.LARGE}>
          Zarezerwuj wizytę
        </ActionButton>
      </DoctorDetailsRight>
    </CardContentWrapper>
  </Card>
)

// -- STYLED
const CardContentWrapper = styled.div`
  display: flex;

  width: 100%;

  @media only screen and (max-width: 767px) {
    flex-direction: column;

    text-align: center;
  }
`
const Category = styled(Heading)`
  font-size: 14px;

  color: #20c0f3;
`
const Specialisation = styled(Paragraph)`
  margin-top: 0px;

  font-size: 14px;

  color: ${props => props.theme.color.muted};
`
const RatingsWrapper = styled.div`
  font-size: 14px;
<<<<<<< HEAD
  font-weight: 500;
=======
  font-family: 'poppinsmedium';

>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70

  display: flex;
  align-items: center;
`
const DetailWrapper = styled(Paragraph)`
  margin-top: 0;

  font-size: 14px;
`
const ActionButton = styled(Button)`
  font-size: 14px;
<<<<<<< HEAD
  font-weight: 500;
=======
  font-family: 'poppinsmedium'
;
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70

  margin-bottom: 10px;

  text-transform: uppercase;
`
const StyledIcon = styled(Icon)`
  margin-right: 10px;
`

const DoctorDetailsLeft = styled.div`
  display: flex;

  @media only screen and (max-width: 767px) {
    flex-direction: column;
  }
`
const DoctorDetailsRight = styled(DoctorDetailsLeft)`
  margin-left: auto;

  flex: 0 0 200px;

  flex-direction: column;

  max-width: 200px;

  @media only screen and (max-width: 767px) {
    text-align: center;

    flex: 0 0 100%;

    max-width: 100%;

    margin-left: 0;
  }
`
const ImageWrapper = styled.div`
  flex: 0 0 150px;
  margin-right: 20px;
  width: 150px;

  @media only screen and (max-width: 767px) {
    margin: 0 auto 20px;
  }
`
const Details = styled.div`
  display: flex;

  flex-direction: column;

  margin-bottom: 15px;

  @media only screen and (max-width: 767px) {
    align-items: center;

    text-align: center;
  }
`
