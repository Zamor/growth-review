import React, { ReactNode, FC } from 'react'
import styled, { css } from 'styled-components'

import { Type } from './constants/type'
import { colorSchemes } from './constants/colorSchemes'

// -- TYPES
type AlertProps = {
  closable?: boolean
  onClose?: () => void
  message: string | ReactNode
  type: Type
}

// -- COMPONENT
export const Alert: FC<AlertProps> = ({
  onClose,
  closable = false,
  message,
  type
}: AlertProps) => (
  <Wrapper style={colorSchemes[type]}>
    {message}
    {closable && <CloseIcon onClick={onClose} iconColor={colorSchemes[type].color} />}
  </Wrapper>
)

// -- STYLES
const Wrapper = styled.div`
  display: flex;

  align-items: baseline;
  justify-content: space-between;

  border: 1px solid ${props => props.theme.color.borderGrey};
  border-radius: 4px;

  padding: 12px 20px;
  margin-bottom: 16px;
`
const HoverCloseIcon = css`
  background-color: ${props => props.theme.color.black};
  transition: background-color 0.4s ease;
`
const HoverIconLines = css`
  position: absolute;
  left: 7px;
  content: ' ';
  height: 14px;
  width: 2px;
  background-color: rgba(0, 0, 0, 0, 0.5);
`

const CloseIcon = styled.div<{ iconColor: string }>`
  width: 13px;
  height: 13px;

  margin-left: 20px;

  position: relative;

  cursor: pointer;

  &:hover {
    &::before, &::after {
      ${HoverCloseIcon}
    }
  }

  &::before {
    ${HoverIconLines}
    transform: rotate(45deg);
    background-color: ${props => props.iconColor};
  }

  &::after {
    ${HoverIconLines}
    transform: rotate(-45deg);
    background-color: ${props => props.iconColor};
  }
`
