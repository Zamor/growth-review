import { theme } from '../../themes/theme'

export const colorSchemes = {
  info: {
    color: theme.color.alert.info,
    backgroundColor: '#cce5ff',
    borderColor: '#b8daff'
  },
  warning: {
    color: theme.color.alert.warning,
    backgroundColor: '#fff3cd',
    borderColor: '#ffeeba'
  },
  success: {
    color: theme.color.alert.success,
    backgroundColor: '#d4edda',
    borderColor: '#c3e6cb'
  },
  error: {
    color: theme.color.alert.error,
    backgroundColor: '#f8d7da',
    borderColor: '#f5c6cb'
  }
}
