import React, { FC } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { IconDefinition } from '@fortawesome/fontawesome-svg-core'

// -- TYPES
type IconProps = {
  iconType: IconDefinition
}

// -- COMPONENT
export const Icon: FC<IconProps> = ({ iconType, ...rest }: IconProps) => (
  <FontAwesomeIcon icon={iconType} {...rest} />
)
