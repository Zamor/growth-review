
export enum Size {
  LG = 16,
  MD = 8,
  SM = 6,
  XS = 4
}
