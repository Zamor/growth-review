import React, { FC } from 'react'
import styled from 'styled-components'

import { Size } from './constants/size'

// -- TYPES
type ProgressBarProps = {
  size?: Size
  bgColor?: string,
  percentageProgress: number,
  striped?: boolean
}

// -- COMPONENT
export const ProgressBar: FC<ProgressBarProps> = ({
  size = Size.LG,
  bgColor = '#007bff',
  percentageProgress,
  striped = false
}: ProgressBarProps) => (
  <BgBar height={size || Size.LG}>
    <Bar width={percentageProgress} bgColor={bgColor} striped={striped} />
  </BgBar>
)

// -- STYLES
const BgBar = styled.div<{ height: number }>`
  width: 100%;
  height: ${props => props.height}px;

  background-color: #e9ecef;

  border-radius: 4px;

  overflow: hidden;
`
const Bar = styled.div<{ width: number, bgColor: string, striped: boolean }>`
  width: ${props => props.width}%;
  height: inherit;

  background-color: ${props => props.bgColor};

  ${props => props.striped && `
    background-size: 16px;
    background-image: linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent);
  `}

`
