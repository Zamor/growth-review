import React, { FC, ReactNode } from 'react'
import styled from 'styled-components'
import Link from 'next/link'

// -- TYPES
interface PathPart {
  url: string
  label: string
}

type BreadcrumbsProps = {
  path: PathPart[]
}

// -- COMPONENT
export const Breadcrumbs: FC<BreadcrumbsProps> = ({
  path
}: BreadcrumbsProps) => {
  const renderItems = (): ReactNode => {
    if (!Array.isArray(path) || !path.length) {
      return null
    }

    return path.map((singleItem, index) => {
      const isDisabled = index === path.length - 1

      return (
        <Link
          href={isDisabled ? singleItem.url : ''}
          key={singleItem.label}
        >
          <BreadcrumbItem disabled={isDisabled}>{singleItem.label}</BreadcrumbItem>
        </Link>
      )
    })
  }

  return (
    <Wrapper>
      <BreadcrumbList>
        {renderItems()}
      </BreadcrumbList>
    </Wrapper>
  )
}

// -- STYLED
const Wrapper = styled.nav`
  padding: 12px 16px;

  margin-bottom: 16px;

  background-color: #e9ecef;

  border-radius: 4px;
`
const BreadcrumbList = styled.ol`
  margin: 0;
  padding: 0;

  a {
    padding: 8px;
  }

  a:first-of-type {
    padding-left: 0px
  }

`
const BreadcrumbItem = styled.a<{ disabled: boolean }>`
  cursor: pointer;

  color: #0dd9f9;

  position: relative;

  ${props => !props.disabled && `
    &::before {
      content: "/";
      position: absolute;
      right: -2px;
      color: #6c757d;
    }
  `}

  ${props => props.disabled && `
    cursor: auto;
    color: #6c757d;
  `}
`
