webpackHotUpdate("static/development/pages/specialist/[spec].js",{

/***/ "./pages/specialist/[spec].tsx":
/*!*************************************!*\
  !*** ./pages/specialist/[spec].tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "../node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "../node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Layout/GridElements */ "./components/Layout/GridElements.tsx");
/* harmony import */ var _components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Layout/CategorySideMenu/CategorySideMenu */ "./components/Layout/CategorySideMenu/CategorySideMenu.tsx");
/* harmony import */ var _components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/Pages/Specialist/SpecialistList */ "./components/Pages/Specialist/SpecialistList.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/pages/specialist/[spec].tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





 // -- COMPONENT

var Specialist = function Specialist() {
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_2__["useRouter"])();
  console.log(router);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_3__["Row"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_3__["Column"], {
    xl: 25,
    lg: 33.3,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_4__["CategorySideMenu"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 9
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ResultColumn, {
    xl: 75,
    lg: 66.6,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_5__["SpecialistList"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  })));
}; // -- STYLED


var ResultColumn = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_3__["Column"]).withConfig({
  displayName: "spec__ResultColumn",
  componentId: "sc-1vlbw7p-0"
})(["flex-direction:column;"]);
/* harmony default export */ __webpack_exports__["default"] = (Specialist);

/***/ })

})
//# sourceMappingURL=[spec].js.e10d5f376bc7ee39f593.hot-update.js.map