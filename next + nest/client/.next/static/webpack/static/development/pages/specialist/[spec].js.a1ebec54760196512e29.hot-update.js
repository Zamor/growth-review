webpackHotUpdate("static/development/pages/specialist/[spec].js",{

/***/ "./pages/specialist/[spec].tsx":
/*!*************************************!*\
  !*** ./pages/specialist/[spec].tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "../node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/Layout/GridElements */ "./components/Layout/GridElements.tsx");
/* harmony import */ var _components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Layout/CategorySideMenu/CategorySideMenu */ "./components/Layout/CategorySideMenu/CategorySideMenu.tsx");
/* harmony import */ var _components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Pages/Specialist/SpecialistList */ "./components/Pages/Specialist/SpecialistList.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/pages/specialist/[spec].tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




 // -- COMPONENT

var Specialist = function Specialist() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 3
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Column"], {
    xl: 25,
    lg: 33.3,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_3__["CategorySideMenu"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ResultColumn, {
    xl: 75,
    lg: 66.6,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_4__["SpecialistList"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }
  })));
}; // -- STYLED


var ResultColumn = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["default"])(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Column"]).withConfig({
  displayName: "spec__ResultColumn",
  componentId: "sc-1vlbw7p-0"
})(["flex-direction:column;"]);
/* harmony default export */ __webpack_exports__["default"] = (Specialist);

/***/ })

})
//# sourceMappingURL=[spec].js.a1ebec54760196512e29.hot-update.js.map