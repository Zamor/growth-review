module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "../next-server/lib/router-context":
/*!**************************************************************!*\
  !*** external "next/dist/next-server/lib/router-context.js" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/router-context.js");

/***/ }),

/***/ "../next-server/lib/utils":
/*!*****************************************************!*\
  !*** external "next/dist/next-server/lib/utils.js" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/dist/next-server/lib/utils.js");

/***/ }),

/***/ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js":
/*!*****************************************************************************************************************!*\
  !*** ../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _objectWithoutProperties; });
/* harmony import */ var _objectWithoutPropertiesLoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./objectWithoutPropertiesLoose */ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutPropertiesLoose.js");

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = Object(_objectWithoutPropertiesLoose__WEBPACK_IMPORTED_MODULE_0__["default"])(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

/***/ }),

/***/ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutPropertiesLoose.js":
/*!**********************************************************************************************************************!*\
  !*** ../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutPropertiesLoose.js ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _objectWithoutPropertiesLoose; });
function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

/***/ }),

/***/ "../node_modules/next/dist/client/link.js":
/*!************************************************!*\
  !*** ../node_modules/next/dist/client/link.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js");

var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

exports.__esModule = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(/*! react */ "react"));

var _url = __webpack_require__(/*! url */ "url");

var _utils = __webpack_require__(/*! ../next-server/lib/utils */ "../next-server/lib/utils");

var _router = _interopRequireDefault(__webpack_require__(/*! ./router */ "../node_modules/next/dist/client/router.js"));

var _router2 = __webpack_require__(/*! ../next-server/lib/router/router */ "../node_modules/next/dist/next-server/lib/router/router.js");

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

function memoizedFormatUrl(formatFunc) {
  var lastHref = null;
  var lastAs = null;
  var lastResult = null;
  return (href, as) => {
    if (lastResult && href === lastHref && as === lastAs) {
      return lastResult;
    }

    var result = formatFunc(href, as);
    lastHref = href;
    lastAs = as;
    lastResult = result;
    return result;
  };
}

function formatUrl(url) {
  return url && typeof url === 'object' ? (0, _utils.formatWithValidation)(url) : url;
}

var observer;
var listeners = new Map();
var IntersectionObserver = false ? undefined : null;
var prefetched = {};

function getObserver() {
  // Return shared instance of IntersectionObserver if already created
  if (observer) {
    return observer;
  } // Only create shared IntersectionObserver if supported in browser


  if (!IntersectionObserver) {
    return undefined;
  }

  return observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (!listeners.has(entry.target)) {
        return;
      }

      var cb = listeners.get(entry.target);

      if (entry.isIntersecting || entry.intersectionRatio > 0) {
        observer.unobserve(entry.target);
        listeners.delete(entry.target);
        cb();
      }
    });
  }, {
    rootMargin: '200px'
  });
}

var listenToIntersections = (el, cb) => {
  var observer = getObserver();

  if (!observer) {
    return () => {};
  }

  observer.observe(el);
  listeners.set(el, cb);
  return () => {
    try {
      observer.unobserve(el);
    } catch (err) {
      console.error(err);
    }

    listeners.delete(el);
  };
};

class Link extends _react.Component {
  constructor(props) {
    super(props);
    this.p = void 0;

    this.cleanUpListeners = () => {};

    this.formatUrls = memoizedFormatUrl((href, asHref) => {
      return {
        href: (0, _router2.addBasePath)(formatUrl(href)),
        as: asHref ? (0, _router2.addBasePath)(formatUrl(asHref)) : asHref
      };
    });

    this.linkClicked = e => {
      var {
        nodeName,
        target
      } = e.currentTarget;

      if (nodeName === 'A' && (target && target !== '_self' || e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var {
        href,
        as
      } = this.formatUrls(this.props.href, this.props.as);

      if (!isLocal(href)) {
        // ignore click if it's outside our scope (e.g. https://google.com)
        return;
      }

      var {
        pathname
      } = window.location;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var {
        scroll
      } = this.props;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      _router.default[this.props.replace ? 'replace' : 'push'](href, as, {
        shallow: this.props.shallow
      }).then(success => {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      });
    };

    if (true) {
      if (props.prefetch) {
        console.warn('Next.js auto-prefetches automatically based on viewport. The prefetch attribute is no longer needed. More: https://err.sh/zeit/next.js/prefetch-true-deprecated');
      }
    }

    this.p = props.prefetch !== false;
  }

  componentWillUnmount() {
    this.cleanUpListeners();
  }

  getPaths() {
    var {
      pathname
    } = window.location;
    var {
      href: parsedHref,
      as: parsedAs
    } = this.formatUrls(this.props.href, this.props.as);
    var resolvedHref = (0, _url.resolve)(pathname, parsedHref);
    return [resolvedHref, parsedAs ? (0, _url.resolve)(pathname, parsedAs) : resolvedHref];
  }

  handleRef(ref) {
    if (this.p && IntersectionObserver && ref && ref.tagName) {
      this.cleanUpListeners();
      var isPrefetched = prefetched[this.getPaths().join( // Join on an invalid URI character
      '%')];

      if (!isPrefetched) {
        this.cleanUpListeners = listenToIntersections(ref, () => {
          this.prefetch();
        });
      }
    }
  } // The function is memoized so that no extra lifecycles are needed
  // as per https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html


  prefetch(options) {
    if (!this.p || true) return; // Prefetch the JSON page if asked (only in the client)

    var paths = this.getPaths(); // We need to handle a prefetch error here since we may be
    // loading with priority which can reject but we don't
    // want to force navigation since this is only a prefetch

    _router.default.prefetch(paths[
    /* href */
    0], paths[
    /* asPath */
    1], options).catch(err => {
      if (true) {
        // rethrow to show invalid URL errors
        throw err;
      }
    });

    prefetched[paths.join( // Join on an invalid URI character
    '%')] = true;
  }

  render() {
    var {
      children
    } = this.props;
    var {
      href,
      as
    } = this.formatUrls(this.props.href, this.props.as); // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

    if (typeof children === 'string') {
      children = _react.default.createElement("a", null, children);
    } // This will return the first child, if multiple are provided it will throw an error


    var child = _react.Children.only(children);

    var props = {
      ref: el => {
        this.handleRef(el);

        if (child && typeof child === 'object' && child.ref) {
          if (typeof child.ref === 'function') child.ref(el);else if (typeof child.ref === 'object') {
            child.ref.current = el;
          }
        }
      },
      onMouseEnter: e => {
        if (child.props && typeof child.props.onMouseEnter === 'function') {
          child.props.onMouseEnter(e);
        }

        this.prefetch({
          priority: true
        });
      },
      onClick: e => {
        if (child.props && typeof child.props.onClick === 'function') {
          child.props.onClick(e);
        }

        if (!e.defaultPrevented) {
          this.linkClicked(e);
        }
      }
    }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
    // defined, we specify the current 'href', so that repetition is not needed by the user

    if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
      props.href = as || href;
    } // Add the ending slash to the paths. So, we can serve the
    // "<page>/index.html" directly.


    if (false) { var rewriteUrlForNextExport; }

    return _react.default.cloneElement(child, props);
  }

}

if (true) {
  var warn = (0, _utils.execOnce)(console.error); // This module gets removed by webpack.IgnorePlugin

  var PropTypes = __webpack_require__(/*! prop-types */ "prop-types");

  var exact = __webpack_require__(/*! prop-types-exact */ "prop-types-exact"); // @ts-ignore the property is supported, when declaring it on the class it outputs an extra bit of code which is not needed.


  Link.propTypes = exact({
    href: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    as: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    prefetch: PropTypes.bool,
    replace: PropTypes.bool,
    shallow: PropTypes.bool,
    passHref: PropTypes.bool,
    scroll: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.element, (props, propName) => {
      var value = props[propName];

      if (typeof value === 'string') {
        warn("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  });
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ "../node_modules/next/dist/client/router.js":
/*!**************************************************!*\
  !*** ../node_modules/next/dist/client/router.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(/*! @babel/runtime/helpers/interopRequireWildcard */ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router2 = _interopRequireWildcard(__webpack_require__(/*! ../next-server/lib/router/router */ "../node_modules/next/dist/next-server/lib/router/router.js"));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(/*! ../next-server/lib/router-context */ "../next-server/lib/router-context");

var _withRouter = _interopRequireDefault(__webpack_require__(/*! ./with-router */ "../node_modules/next/dist/client/with-router.js"));

exports.withRouter = _withRouter.default;
/* global window */

var singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

var urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath'];
var routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
var coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      var router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = function () {
    var router = getRouter();
    return router[field](...arguments);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, function () {
      var eventField = "on" + event.charAt(0).toUpperCase() + event.substring(1);
      var _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...arguments);
        } catch (err) {
          // tslint:disable-next-line:no-console
          console.error("Error when running the Router event: " + eventField); // tslint:disable-next-line:no-console

          console.error(err.message + "\n" + err.stack);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    var message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


var createRouter = function createRouter() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  var _router = router;
  var instance = {};

  for (var property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign({}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = function () {
      return _router[field](...arguments);
    };
  });
  return instance;
}

/***/ }),

/***/ "../node_modules/next/dist/client/with-router.js":
/*!*******************************************************!*\
  !*** ../node_modules/next/dist/client/with-router.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(/*! @babel/runtime/helpers/interopRequireDefault */ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js");

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__(/*! react */ "react"));

var _router = __webpack_require__(/*! ./router */ "../node_modules/next/dist/client/router.js");

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return _react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (true) {
    var name = ComposedComponent.displayName || ComposedComponent.name || 'Unknown';
    WithRouterWrapper.displayName = "withRouter(" + name + ")";
  }

  return WithRouterWrapper;
}

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/mitt.js":
/*!*********************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/mitt.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/*
MIT License

Copyright (c) Jason Miller (https://jasonformat.com/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

Object.defineProperty(exports, "__esModule", {
  value: true
});

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        // tslint:disable-next-line:no-bitwise
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

exports.default = mitt;

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/router/router.js":
/*!******************************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/router/router.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");

const mitt_1 = __importDefault(__webpack_require__(/*! ../mitt */ "../node_modules/next/dist/next-server/lib/mitt.js"));

const utils_1 = __webpack_require__(/*! ../utils */ "../node_modules/next/dist/next-server/lib/utils.js");

const is_dynamic_1 = __webpack_require__(/*! ./utils/is-dynamic */ "../node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js");

const route_matcher_1 = __webpack_require__(/*! ./utils/route-matcher */ "../node_modules/next/dist/next-server/lib/router/utils/route-matcher.js");

const route_regex_1 = __webpack_require__(/*! ./utils/route-regex */ "../node_modules/next/dist/next-server/lib/router/utils/route-regex.js");

const basePath =  false || '';

function addBasePath(path) {
  return path.indexOf(basePath) !== 0 ? basePath + path : path;
}

exports.addBasePath = addBasePath;

function delBasePath(path) {
  return path.indexOf(basePath) === 0 ? path.substr(basePath.length) || '/' : path;
}

exports.delBasePath = delBasePath;

function toRoute(path) {
  return path.replace(/\/$/, '') || '/';
}

const prepareRoute = path => toRoute(!path || path === '/' ? '/index' : path);

function fetchNextData(pathname, query, isServerRender, cb) {
  let attempts = isServerRender ? 3 : 1;

  function getResponse() {
    return fetch(utils_1.formatWithValidation({
      pathname: addBasePath( // @ts-ignore __NEXT_DATA__
      `/_next/data/${__NEXT_DATA__.buildId}${delBasePath(pathname)}.json`),
      query
    }), {
      // Cookies are required to be present for Next.js' SSG "Preview Mode".
      // Cookies may also be required for `getServerSideProps`.
      //
      // > `fetch` won’t send cookies, unless you set the credentials init
      // > option.
      // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
      //
      // > For maximum browser compatibility when it comes to sending &
      // > receiving cookies, always supply the `credentials: 'same-origin'`
      // > option instead of relying on the default.
      // https://github.com/github/fetch#caveats
      credentials: 'same-origin'
    }).then(res => {
      if (!res.ok) {
        if (--attempts > 0 && res.status >= 500) {
          return getResponse();
        }

        throw new Error(`Failed to load static props`);
      }

      return res.json();
    });
  }

  return getResponse().then(data => {
    return cb ? cb(data) : data;
  }).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      ;
      err.code = 'PAGE_LOAD_ERROR';
    }

    throw err;
  });
}

class Router {
  constructor(pathname, query, as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback
  }) {
    // Static Data Cache
    this.sdc = {};

    this.onPopState = e => {
      if (!e.state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', utils_1.formatWithValidation({
          pathname,
          query
        }), utils_1.getURL());
        return;
      } // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site


      if (e.state && this.isSsr && e.state.as === this.asPath && url_1.parse(e.state.url).pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(e.state)) {
        return;
      }

      const {
        url,
        as,
        options
      } = e.state;

      if (true) {
        if (typeof url === 'undefined' || typeof as === 'undefined') {
          console.warn('`popstate` event triggered but `event.state` did not have `url` or `as` https://err.sh/zeit/next.js/popstate-state-empty');
        }
      }

      this.replace(url, as, options);
    };

    this._getStaticData = asPath => {
      const pathname = prepareRoute(url_1.parse(asPath).pathname);
      return  false ? undefined : fetchNextData(pathname, null, this.isSsr, data => this.sdc[pathname] = data);
    };

    this._getServerData = asPath => {
      let {
        pathname,
        query
      } = url_1.parse(asPath, true);
      pathname = prepareRoute(pathname);
      return fetchNextData(pathname, query, this.isSsr);
    }; // represents the current component key


    this.route = toRoute(pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = pathname;
    this.query = query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    this.asPath = // @ts-ignore this is temporarily global (attached to window)
    is_dynamic_1.isDynamicRoute(pathname) && __NEXT_DATA__.autoExport ? pathname : as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;

    if (false) {}
  } // @deprecated backwards compatibility even though it's a private method.


  static _rewriteUrlForNextExport(url) {
    if (false) {} else {
      return url;
    }
  }

  update(route, mod) {
    const Component = mod.default || mod;
    const data = this.components[route];

    if (!data) {
      throw new Error(`Cannot update unavailable route: ${route}`);
    }

    const newData = Object.assign(Object.assign({}, data), {
      Component,
      __N_SSG: mod.__N_SSG,
      __N_SSP: mod.__N_SSP
    });
    this.components[route] = newData; // pages/_app.js updated

    if (route === '/_app') {
      this.notify(this.components[this.route]);
      return;
    }

    if (route === this.route) {
      this.notify(newData);
    }
  }

  reload() {
    window.location.reload();
  }
  /**
   * Go back in history
   */


  back() {
    window.history.back();
  }
  /**
   * Performs a `pushState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  push(url, as = url, options = {}) {
    return this.change('pushState', url, as, options);
  }
  /**
   * Performs a `replaceState` with arguments
   * @param url of the route
   * @param as masks `url` for the browser
   * @param options object you can define `shallow` and other options
   */


  replace(url, as = url, options = {}) {
    return this.change('replaceState', url, as, options);
  }

  change(method, _url, _as, options) {
    return new Promise((resolve, reject) => {
      if (!options._h) {
        this.isSsr = false;
      } // marking route changes as a navigation start entry


      if (utils_1.ST) {
        performance.mark('routeChange');
      } // If url and as provided as an object representation,
      // we'll format them into the string version here.


      let url = typeof _url === 'object' ? utils_1.formatWithValidation(_url) : _url;
      let as = typeof _as === 'object' ? utils_1.formatWithValidation(_as) : _as;
      url = addBasePath(url);
      as = addBasePath(as); // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly for the SSR page.

      if (false) {}

      this.abortComponentLoad(as); // If the url change is only related to a hash change
      // We should not proceed. We should only change the state.
      // WARNING: `_h` is an internal option for handing Next.js client-side
      // hydration. Your app should _never_ use this property. It may change at
      // any time without notice.

      if (!options._h && this.onlyAHashChange(as)) {
        this.asPath = as;
        Router.events.emit('hashChangeStart', as);
        this.changeState(method, url, as, options);
        this.scrollToHash(as);
        Router.events.emit('hashChangeComplete', as);
        return resolve(true);
      }

      const {
        pathname,
        query,
        protocol
      } = url_1.parse(url, true);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return resolve(false);
      } // If asked to change the current URL we should reload the current page
      // (not location.reload() but reload getInitialProps and other Next.js stuffs)
      // We also need to set the method = replaceState always
      // as this should not go into the history (That's how browsers work)
      // We should compare the new asPath to the current asPath, not the url


      if (!this.urlIsNew(as)) {
        method = 'replaceState';
      }

      const route = toRoute(pathname);
      const {
        shallow = false
      } = options;

      if (is_dynamic_1.isDynamicRoute(route)) {
        const {
          pathname: asPathname
        } = url_1.parse(as);
        const routeRegex = route_regex_1.getRouteRegex(route);
        const routeMatch = route_matcher_1.getRouteMatcher(routeRegex)(asPathname);

        if (!routeMatch) {
          const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

          if (missingParams.length > 0) {
            if (true) {
              console.warn(`Mismatching \`as\` and \`href\` failed to manually provide ` + `the params: ${missingParams.join(', ')} in the \`href\`'s \`query\``);
            }

            return reject(new Error(`The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). ` + `Read more: https://err.sh/zeit/next.js/incompatible-href-as`));
          }
        } else {
          // Merge params into `query`, overwriting any specified in search
          Object.assign(query, routeMatch);
        }
      }

      Router.events.emit('routeChangeStart', as); // If shallow is true and the route exists in the router cache we reuse the previous result

      this.getRouteInfo(route, pathname, query, as, shallow).then(routeInfo => {
        const {
          error
        } = routeInfo;

        if (error && error.cancelled) {
          return resolve(false);
        }

        Router.events.emit('beforeHistoryChange', as);
        this.changeState(method, url, as, options);

        if (true) {
          const appComp = this.components['/_app'].Component;
          window.next.isPrerendered = appComp.getInitialProps === appComp.origGetInitialProps && !routeInfo.Component.getInitialProps;
        }

        this.set(route, pathname, query, as, routeInfo);

        if (error) {
          Router.events.emit('routeChangeError', error, as);
          throw error;
        }

        Router.events.emit('routeChangeComplete', as);
        return resolve(true);
      }, reject);
    });
  }

  changeState(method, url, as, options = {}) {
    if (true) {
      if (typeof window.history === 'undefined') {
        console.error(`Warning: window.history is not available.`);
        return;
      }

      if (typeof window.history[method] === 'undefined') {
        console.error(`Warning: window.history.${method} is not available`);
        return;
      }
    }

    if (method !== 'pushState' || utils_1.getURL() !== as) {
      window.history[method]({
        url,
        as,
        options
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  getRouteInfo(route, pathname, query, as, shallow = false) {
    const cachedRouteInfo = this.components[route]; // If there is a shallow route transition possible
    // If the route is already rendered on the screen.

    if (shallow && cachedRouteInfo && this.route === route) {
      return Promise.resolve(cachedRouteInfo);
    }

    const handleError = (err, loadErrorFail) => {
      return new Promise(resolve => {
        if (err.code === 'PAGE_LOAD_ERROR' || loadErrorFail) {
          // If we can't load the page it could be one of following reasons
          //  1. Page doesn't exists
          //  2. Page does exist in a different zone
          //  3. Internal error while loading the page
          // So, doing a hard reload is the proper way to deal with this.
          window.location.href = as; // Changing the URL doesn't block executing the current code path.
          // So, we need to mark it as a cancelled error and stop the routing logic.

          err.cancelled = true; // @ts-ignore TODO: fix the control flow here

          return resolve({
            error: err
          });
        }

        if (err.cancelled) {
          // @ts-ignore TODO: fix the control flow here
          return resolve({
            error: err
          });
        }

        resolve(this.fetchComponent('/_error').then(res => {
          const {
            page: Component
          } = res;
          const routeInfo = {
            Component,
            err
          };
          return new Promise(resolve => {
            this.getInitialProps(Component, {
              err,
              pathname,
              query
            }).then(props => {
              routeInfo.props = props;
              routeInfo.error = err;
              resolve(routeInfo);
            }, gipErr => {
              console.error('Error in error page `getInitialProps`: ', gipErr);
              routeInfo.error = err;
              routeInfo.props = {};
              resolve(routeInfo);
            });
          });
        }).catch(err => handleError(err, true)));
      });
    };

    return new Promise((resolve, reject) => {
      if (cachedRouteInfo) {
        return resolve(cachedRouteInfo);
      }

      this.fetchComponent(route).then(res => resolve({
        Component: res.page,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }), reject);
    }).then(routeInfo => {
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (true) {
        const {
          isValidElementType
        } = __webpack_require__(/*! react-is */ "../node_modules/next/node_modules/react-is/index.js");

        if (!isValidElementType(Component)) {
          throw new Error(`The default export is not a React Component in page: "${pathname}"`);
        }
      }

      return this._getData(() => __N_SSG ? this._getStaticData(as) : __N_SSP ? this._getServerData(as) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      })).then(props => {
        routeInfo.props = props;
        this.components[route] = routeInfo;
        return routeInfo;
      });
    }).catch(handleError);
  }

  set(route, pathname, query, as, data) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    this.notify(data);
  }
  /**
   * Callback to execute before replacing router state
   * @param cb callback to be executed
   */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value

    if (hash === '') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
   * Prefetch page code, you may wait for the data during page rendering.
   * This feature only works in production!
   * @param url the href of prefetched page
   * @param asPath the as path of the prefetched page
   */


  prefetch(url, asPath = url, options = {}) {
    return new Promise((resolve, reject) => {
      const {
        pathname,
        protocol
      } = url_1.parse(url);

      if (!pathname || protocol) {
        if (true) {
          throw new Error(`Invalid href passed to router: ${url} https://err.sh/zeit/next.js/invalid-href-passed`);
        }

        return;
      } // Prefetch is not supported in development mode because it would trigger on-demand-entries


      if (true) {
        return;
      }

      const route = delBasePath(toRoute(pathname));
      Promise.all([this.pageLoader.prefetchData(url, delBasePath(asPath)), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]).then(() => resolve(), reject);
    });
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    route = delBasePath(route);
    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return utils_1.loadGetInitialProps(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as) {
    if (this.clc) {
      const e = new Error('Route Cancelled');
      e.cancelled = true;
      Router.events.emit('routeChangeError', e, as);
      this.clc();
      this.clc = null;
    }
  }

  notify(data) {
    this.sub(data, this.components['/_app'].Component);
  }

}

exports.default = Router;
Router.events = mitt_1.default();

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js":
/*!****************************************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/router/utils/is-dynamic.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

exports.isDynamicRoute = isDynamicRoute;

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/router/utils/route-matcher.js":
/*!*******************************************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/router/utils/route-matcher.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

exports.getRouteMatcher = getRouteMatcher;

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/router/utils/route-regex.js":
/*!*****************************************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/router/utils/route-regex.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
}); // this isn't importing the escape-string-regex module
// to reduce bytes

function escapeRegex(str) {
  return str.replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
}

function getRouteRegex(normalizedRoute) {
  // Escape all characters that could be considered RegEx
  const escapedRoute = escapeRegex(normalizedRoute.replace(/\/$/, '') || '/');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
    const isCatchAll = /^(\\\.){3}/.test($1);
    groups[$1 // Un-escape key
    .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '') // eslint-disable-next-line no-sequences
    ] = {
      pos: groupIndex++,
      repeat: isCatchAll
    };
    return isCatchAll ? '/(.+?)' : '/([^/]+?)';
  });
  let namedParameterizedRoute; // dead code eliminate for browser since it's only needed
  // while generating routes-manifest

  if (true) {
    namedParameterizedRoute = escapedRoute.replace(/\/\\\[([^/]+?)\\\](?=\/|$)/g, (_, $1) => {
      const isCatchAll = /^(\\\.){3}/.test($1);
      const key = $1 // Un-escape key
      .replace(/\\([|\\{}()[\]^$+*?.-])/g, '$1').replace(/^\.{3}/, '');
      return isCatchAll ? `/(?<${escapeRegex(key)}>.+?)` : `/(?<${escapeRegex(key)}>[^/]+?)`;
    });
  }

  return Object.assign({
    re: new RegExp('^' + parameterizedRoute + '(?:/)?$', 'i'),
    groups
  }, namedParameterizedRoute ? {
    namedRegex: `^${namedParameterizedRoute}(?:/)?$`
  } : {});
}

exports.getRouteRegex = getRouteRegex;

/***/ }),

/***/ "../node_modules/next/dist/next-server/lib/utils.js":
/*!**********************************************************!*\
  !*** ../node_modules/next/dist/next-server/lib/utils.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__(/*! url */ "url");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  var _a;

  if (true) {
    if ((_a = App.prototype) === null || _a === void 0 ? void 0 : _a.getInitialProps) {
      const message = `"${getDisplayName(App)}.getInitialProps()" is defined as an instance method - visit https://err.sh/zeit/next.js/get-initial-props-as-an-instance-method for more information.`;
      throw new Error(message);
    }
  } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (true) {
    if (Object.keys(props).length === 0 && !ctx.ctx) {
      console.warn(`${getDisplayName(App)} returned an empty object from \`getInitialProps\`. This de-optimizes and prevents automatic static optimization. https://err.sh/zeit/next.js/empty-object-getInitialProps`);
    }
  }

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (true) {
    if (url !== null && typeof url === 'object') {
      Object.keys(url).forEach(key => {
        if (exports.urlObjectKeys.indexOf(key) === -1) {
          console.warn(`Unknown key passed via urlObject into url.format: ${key}`);
        }
      });
    }
  }

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SP = typeof performance !== 'undefined';
exports.ST = exports.SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "../node_modules/next/link.js":
/*!************************************!*\
  !*** ../node_modules/next/link.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./dist/client/link */ "../node_modules/next/dist/client/link.js")


/***/ }),

/***/ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js":
/*!*****************************************************************************************!*\
  !*** ../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireDefault.js ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireWildcard.js":
/*!******************************************************************************************!*\
  !*** ../node_modules/next/node_modules/@babel/runtime/helpers/interopRequireWildcard.js ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ../helpers/typeof */ "../node_modules/next/node_modules/@babel/runtime/helpers/typeof.js");

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ "../node_modules/next/node_modules/@babel/runtime/helpers/typeof.js":
/*!**************************************************************************!*\
  !*** ../node_modules/next/node_modules/@babel/runtime/helpers/typeof.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof2(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof2 = function _typeof2(obj) { return typeof obj; }; } else { _typeof2 = function _typeof2(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof2(obj); }

function _typeof(obj) {
  if (typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return _typeof2(obj);
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : _typeof2(obj);
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "../node_modules/next/node_modules/react-is/cjs/react-is.development.js":
/*!******************************************************************************!*\
  !*** ../node_modules/next/node_modules/react-is/cjs/react-is.development.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v16.8.6
 * react-is.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */





if (true) {
  (function() {
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var hasSymbol = typeof Symbol === 'function' && Symbol.for;

var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for('react.element') : 0xeac7;
var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for('react.portal') : 0xeaca;
var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for('react.fragment') : 0xeacb;
var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for('react.strict_mode') : 0xeacc;
var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for('react.profiler') : 0xead2;
var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for('react.provider') : 0xeacd;
var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for('react.context') : 0xeace;
var REACT_ASYNC_MODE_TYPE = hasSymbol ? Symbol.for('react.async_mode') : 0xeacf;
var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for('react.concurrent_mode') : 0xeacf;
var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for('react.forward_ref') : 0xead0;
var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for('react.suspense') : 0xead1;
var REACT_MEMO_TYPE = hasSymbol ? Symbol.for('react.memo') : 0xead3;
var REACT_LAZY_TYPE = hasSymbol ? Symbol.for('react.lazy') : 0xead4;

function isValidElementType(type) {
  return typeof type === 'string' || typeof type === 'function' ||
  // Note: its typeof might be other than 'symbol' or 'number' if it's a polyfill.
  type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || typeof type === 'object' && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE);
}

/**
 * Forked from fbjs/warning:
 * https://github.com/facebook/fbjs/blob/e66ba20ad5be433eb54423f2b097d829324d9de6/packages/fbjs/src/__forks__/warning.js
 *
 * Only change is we use console.warn instead of console.error,
 * and do nothing when 'console' is not supported.
 * This really simplifies the code.
 * ---
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var lowPriorityWarning = function () {};

{
  var printWarning = function (format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.warn(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  lowPriorityWarning = function (condition, format) {
    if (format === undefined) {
      throw new Error('`lowPriorityWarning(condition, format, ...args)` requires a warning ' + 'message argument');
    }
    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

var lowPriorityWarning$1 = lowPriorityWarning;

function typeOf(object) {
  if (typeof object === 'object' && object !== null) {
    var $$typeof = object.$$typeof;
    switch ($$typeof) {
      case REACT_ELEMENT_TYPE:
        var type = object.type;

        switch (type) {
          case REACT_ASYNC_MODE_TYPE:
          case REACT_CONCURRENT_MODE_TYPE:
          case REACT_FRAGMENT_TYPE:
          case REACT_PROFILER_TYPE:
          case REACT_STRICT_MODE_TYPE:
          case REACT_SUSPENSE_TYPE:
            return type;
          default:
            var $$typeofType = type && type.$$typeof;

            switch ($$typeofType) {
              case REACT_CONTEXT_TYPE:
              case REACT_FORWARD_REF_TYPE:
              case REACT_PROVIDER_TYPE:
                return $$typeofType;
              default:
                return $$typeof;
            }
        }
      case REACT_LAZY_TYPE:
      case REACT_MEMO_TYPE:
      case REACT_PORTAL_TYPE:
        return $$typeof;
    }
  }

  return undefined;
}

// AsyncMode is deprecated along with isAsyncMode
var AsyncMode = REACT_ASYNC_MODE_TYPE;
var ConcurrentMode = REACT_CONCURRENT_MODE_TYPE;
var ContextConsumer = REACT_CONTEXT_TYPE;
var ContextProvider = REACT_PROVIDER_TYPE;
var Element = REACT_ELEMENT_TYPE;
var ForwardRef = REACT_FORWARD_REF_TYPE;
var Fragment = REACT_FRAGMENT_TYPE;
var Lazy = REACT_LAZY_TYPE;
var Memo = REACT_MEMO_TYPE;
var Portal = REACT_PORTAL_TYPE;
var Profiler = REACT_PROFILER_TYPE;
var StrictMode = REACT_STRICT_MODE_TYPE;
var Suspense = REACT_SUSPENSE_TYPE;

var hasWarnedAboutDeprecatedIsAsyncMode = false;

// AsyncMode should be deprecated
function isAsyncMode(object) {
  {
    if (!hasWarnedAboutDeprecatedIsAsyncMode) {
      hasWarnedAboutDeprecatedIsAsyncMode = true;
      lowPriorityWarning$1(false, 'The ReactIs.isAsyncMode() alias has been deprecated, ' + 'and will be removed in React 17+. Update your code to use ' + 'ReactIs.isConcurrentMode() instead. It has the exact same API.');
    }
  }
  return isConcurrentMode(object) || typeOf(object) === REACT_ASYNC_MODE_TYPE;
}
function isConcurrentMode(object) {
  return typeOf(object) === REACT_CONCURRENT_MODE_TYPE;
}
function isContextConsumer(object) {
  return typeOf(object) === REACT_CONTEXT_TYPE;
}
function isContextProvider(object) {
  return typeOf(object) === REACT_PROVIDER_TYPE;
}
function isElement(object) {
  return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
}
function isForwardRef(object) {
  return typeOf(object) === REACT_FORWARD_REF_TYPE;
}
function isFragment(object) {
  return typeOf(object) === REACT_FRAGMENT_TYPE;
}
function isLazy(object) {
  return typeOf(object) === REACT_LAZY_TYPE;
}
function isMemo(object) {
  return typeOf(object) === REACT_MEMO_TYPE;
}
function isPortal(object) {
  return typeOf(object) === REACT_PORTAL_TYPE;
}
function isProfiler(object) {
  return typeOf(object) === REACT_PROFILER_TYPE;
}
function isStrictMode(object) {
  return typeOf(object) === REACT_STRICT_MODE_TYPE;
}
function isSuspense(object) {
  return typeOf(object) === REACT_SUSPENSE_TYPE;
}

exports.typeOf = typeOf;
exports.AsyncMode = AsyncMode;
exports.ConcurrentMode = ConcurrentMode;
exports.ContextConsumer = ContextConsumer;
exports.ContextProvider = ContextProvider;
exports.Element = Element;
exports.ForwardRef = ForwardRef;
exports.Fragment = Fragment;
exports.Lazy = Lazy;
exports.Memo = Memo;
exports.Portal = Portal;
exports.Profiler = Profiler;
exports.StrictMode = StrictMode;
exports.Suspense = Suspense;
exports.isValidElementType = isValidElementType;
exports.isAsyncMode = isAsyncMode;
exports.isConcurrentMode = isConcurrentMode;
exports.isContextConsumer = isContextConsumer;
exports.isContextProvider = isContextProvider;
exports.isElement = isElement;
exports.isForwardRef = isForwardRef;
exports.isFragment = isFragment;
exports.isLazy = isLazy;
exports.isMemo = isMemo;
exports.isPortal = isPortal;
exports.isProfiler = isProfiler;
exports.isStrictMode = isStrictMode;
exports.isSuspense = isSuspense;
  })();
}


/***/ }),

/***/ "../node_modules/next/node_modules/react-is/index.js":
/*!***********************************************************!*\
  !*** ../node_modules/next/node_modules/react-is/index.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (false) {} else {
  module.exports = __webpack_require__(/*! ./cjs/react-is.development.js */ "../node_modules/next/node_modules/react-is/cjs/react-is.development.js");
}


/***/ }),

/***/ "./components/Avatar/Avatar.tsx":
/*!**************************************!*\
  !*** ./components/Avatar/Avatar.tsx ***!
  \**************************************/
/*! exports provided: Avatar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Avatar", function() { return Avatar; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants_size__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./constants/size */ "./components/Avatar/constants/size.ts");
/* harmony import */ var _constants_shape__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants/shape */ "./components/Avatar/constants/shape.ts");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Avatar/Avatar.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



 // -- TYPES

// -- COMPONENT
var Avatar = function Avatar(_ref) {
  var _ref$size = _ref.size,
      size = _ref$size === void 0 ? _constants_size__WEBPACK_IMPORTED_MODULE_2__["Size"].L : _ref$size,
      image = _ref.image,
      status = _ref.status,
      _ref$shape = _ref.shape,
      shape = _ref$shape === void 0 ? _constants_shape__WEBPACK_IMPORTED_MODULE_3__["Shape"].CIRCLE : _ref$shape;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Wrapper, {
    size: size,
    status: status,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 3
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AvatarImage, {
    src: image,
    alt: "opcjonalna fotka",
    shape: shape,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 5
    }
  }));
}; // -- STYLED

var Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Avatar__Wrapper",
  componentId: "ssmdfb-0"
})(["width:", "px;height:", "px;position:relative;", ""], function (props) {
  return props.size;
}, function (props) {
  return props.size;
}, function (props) {
  return props.status && "\n    &::before {\n      position: absolute;\n\n      right: 0;\n      bottom: 0;\n\n      width: 25%;\n      height: 25%;\n\n      border-radius: 50%;\n      border: 2px solid #fff;\n\n      content: '';\n\n      background-color: ".concat(props.theme.color[props.status], "\n    }\n  ");
});
var AvatarImage = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.img.withConfig({
  displayName: "Avatar__AvatarImage",
  componentId: "ssmdfb-1"
})(["width:100%;height:100%;border-radius:", ";object-fit:cover;"], function (props) {
  return props.shape === 'circle' ? '50%' : '6px';
});

/***/ }),

/***/ "./components/Avatar/constants/shape.ts":
/*!**********************************************!*\
  !*** ./components/Avatar/constants/shape.ts ***!
  \**********************************************/
/*! exports provided: Shape */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Shape", function() { return Shape; });
var Shape;

(function (Shape) {
  Shape["CIRCLE"] = "circle";
  Shape["ROUNDED"] = "rounded";
})(Shape || (Shape = {}));

/***/ }),

/***/ "./components/Avatar/constants/size.ts":
/*!*********************************************!*\
  !*** ./components/Avatar/constants/size.ts ***!
  \*********************************************/
/*! exports provided: Size */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Size", function() { return Size; });
var Size;

(function (Size) {
  Size[Size["XXL"] = 128] = "XXL";
  Size[Size["XL"] = 80] = "XL";
  Size[Size["L"] = 60] = "L";
  Size[Size["S"] = 48] = "S";
})(Size || (Size = {}));

/***/ }),

/***/ "./components/Button/Button.tsx":
/*!**************************************!*\
  !*** ./components/Button/Button.tsx ***!
  \**************************************/
/*! exports provided: Button */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Button", function() { return Button; });
/* harmony import */ var _home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _constants_color__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./constants/color */ "./components/Button/constants/color.ts");
/* harmony import */ var _constants_size__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./constants/size */ "./components/Button/constants/size.ts");


var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Button/Button.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



 // -- TYPES

// -- COMPONENT
var Button = function Button(_ref) {
  var _ref$color = _ref.color,
      color = _ref$color === void 0 ? _constants_color__WEBPACK_IMPORTED_MODULE_3__["Color"].PRIMARY : _ref$color,
      _ref$rounded = _ref.rounded,
      rounded = _ref$rounded === void 0 ? false : _ref$rounded,
      _ref$outline = _ref.outline,
      outline = _ref$outline === void 0 ? false : _ref$outline,
      _ref$disabled = _ref.disabled,
      disabled = _ref$disabled === void 0 ? false : _ref$disabled,
      _ref$isLoading = _ref.isLoading,
      isLoading = _ref$isLoading === void 0 ? false : _ref$isLoading,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? _constants_size__WEBPACK_IMPORTED_MODULE_4__["Size"].NORMAL : _ref$size,
      children = _ref.children,
      rest = Object(_home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_ref, ["color", "rounded", "outline", "disabled", "isLoading", "size", "children"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(ButtonElement, Object.assign({
    color: color,
    rounded: rounded,
    outline: outline,
    disabled: disabled,
    size: size
  }, rest, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 3
    }
  }), isLoading && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(Loader, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 19
    }
  }), children);
}; // -- STYLED

var ButtonElement = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.button.withConfig({
  displayName: "Button__ButtonElement",
  componentId: "pf0pwk-0"
})(["display:flex;align-items:center;justify-content:center;padding:6px 12px;color:", ";background-color:", ";border:1px solid ", ";border-radius:4px;cursor:pointer;", " ", " ", " ", " ", ""], function (props) {
  return props.theme.color.white;
}, function (props) {
  return props.theme.color.button[props.color];
}, function (props) {
  return props.theme.color.button[props.color];
}, function (props) {
  return props.outline && "\n    background-color: ".concat(props.theme.color.white, ";\n\n    color: ").concat(props.theme.color.button[props.color], ";\n\n    border-color: ").concat(props.theme.color.button[props.color], ";\n\n    &:hover {\n      color: ").concat(props.theme.color.white, ";\n\n      background-color: ").concat(props.theme.color.button[props.color], ";\n    }\n  ");
}, function (props) {
  return props.rounded && "\n    border-radius: 50px;\n  ";
}, function (props) {
  return props.disabled && "\n    background-color: #f8f9fa;\n\n    color: #a6a6a6;\n\n    border-color: #e6e6e6;\n\n    cursor: not-allowed;\n  ";
}, function (props) {
  return props.size === _constants_size__WEBPACK_IMPORTED_MODULE_4__["Size"].LARGE && "\n    font-size: 20px;\n\n    padding: 8px 16px;\n  ";
}, function (props) {
  return props.size === _constants_size__WEBPACK_IMPORTED_MODULE_4__["Size"].SMALL && "\n    font-size: 14px;\n\n    padding: 4px 8px;\n  ";
});
var Animation = Object(styled_components__WEBPACK_IMPORTED_MODULE_2__["keyframes"])(["0%{transform:rotate(0deg);}100%{transform:rotate(360deg);}"]);
var Loader = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div.withConfig({
  displayName: "Button__Loader",
  componentId: "pf0pwk-1"
})(["margin-right:10px;border:3px solid ", ";border-radius:50%;border-top:3px solid rgba(0,0,0,0);width:10px;height:10px;animation:", " 1s linear infinite;"], function (props) {
  return props.theme.color.white;
}, Animation);

/***/ }),

/***/ "./components/Button/constants/color.ts":
/*!**********************************************!*\
  !*** ./components/Button/constants/color.ts ***!
  \**********************************************/
/*! exports provided: Color */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Color", function() { return Color; });
var Color;

(function (Color) {
  Color["PRIMARY"] = "primary";
  Color["SECONDARY"] = "secondary";
  Color["SUCCESS"] = "success";
  Color["DANGER"] = "danger";
  Color["WARNING"] = "warning";
  Color["INFO"] = "info";
})(Color || (Color = {}));

/***/ }),

/***/ "./components/Button/constants/size.ts":
/*!*********************************************!*\
  !*** ./components/Button/constants/size.ts ***!
  \*********************************************/
/*! exports provided: Size */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Size", function() { return Size; });
var Size;

(function (Size) {
  Size["LARGE"] = "large";
  Size["NORMAL"] = "normal";
  Size["SMALL"] = "small";
})(Size || (Size = {}));

/***/ }),

/***/ "./components/Card.tsx":
/*!*****************************!*\
  !*** ./components/Card.tsx ***!
  \*****************************/
/*! exports provided: Card */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Card", function() { return Card; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Card.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

 // -- TYPES

// -- COMPONENT
var Card = function Card(_ref) {
  var imageUrl = _ref.imageUrl,
      header = _ref.header,
      children = _ref.children,
      footer = _ref.footer;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Wrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 3
    }
  }, imageUrl && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HeaderImage, {
    src: imageUrl,
    alt: "Card image",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }
  }), header && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Header, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 7
    }
  }, header), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Content, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 5
    }
  }, children), footer && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Footer, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 7
    }
  }, footer));
}; // -- STYLED

var Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Card__Wrapper",
  componentId: "sc-11netv2-0"
})(["display:flex;flex-direction:column;border:1px solid ", ";border-radius:4px;margin-bottom:30px;background-color:", ";width:100%;"], function (props) {
  return props.theme.color.borderGrey;
}, function (props) {
  return props.theme.color.white;
});
var HeaderImage = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.img.withConfig({
  displayName: "Card__HeaderImage",
  componentId: "sc-11netv2-1"
})(["width:100%;"]);
var Header = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.h5.withConfig({
  displayName: "Card__Header",
  componentId: "sc-11netv2-2"
})(["padding:16px 24px;margin:0;border-bottom:1px solid ", ";font-size:20px;font-family:'poppinsmedium';"], function (props) {
  return props.theme.color.borderGrey;
});
var Content = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Card__Content",
  componentId: "sc-11netv2-3"
})(["padding:24px;"]);
var Footer = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "Card__Footer",
  componentId: "sc-11netv2-4"
})(["border-top:1px solid ", ";padding:16px 24px;color:", ";"], function (props) {
  return props.theme.color.borderGrey;
}, function (props) {
  return props.theme.color.muted;
});

/***/ }),

/***/ "./components/Heading.tsx":
/*!********************************!*\
  !*** ./components/Heading.tsx ***!
  \********************************/
/*! exports provided: Heading */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Heading", function() { return Heading; });
/* harmony import */ var _home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);


var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Heading.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;

 // -- TYPES

// -- CONF
var sizeConf = {
  1: 40,
  2: 32,
  3: 28,
  4: 24,
  5: 20,
  6: 16
}; // -- COMPONENTS

var Heading = Object(react__WEBPACK_IMPORTED_MODULE_1__["memo"])(function (_ref) {
  var children = _ref.children,
      level = _ref.level,
      rest = Object(_home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_ref, ["children", "level"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(H, Object.assign({
    as: "h".concat(level),
    headingLevel: level
  }, rest, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 3
    }
  }), children);
}); // -- STYLES

var H = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.h1.withConfig({
  displayName: "Heading__H",
  componentId: "pyp2bh-0"
})(["margin:0 0 8px 0;font-size:", "px;"], function (props) {
  return sizeConf[props.headingLevel];
});

/***/ }),

/***/ "./components/Icon.tsx":
/*!*****************************!*\
  !*** ./components/Icon.tsx ***!
  \*****************************/
/*! exports provided: Icon */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Icon", function() { return Icon; });
/* harmony import */ var _home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties */ "../node_modules/babel-preset-react-app/node_modules/@babel/runtime/helpers/esm/objectWithoutProperties.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/react-fontawesome */ "@fortawesome/react-fontawesome");
/* harmony import */ var _fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__);


var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Icon.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;


// -- COMPONENT
var Icon = function Icon(_ref) {
  var iconType = _ref.iconType,
      rest = Object(_home_przemyslaw_Pulpit_moje_randlab_alfacare_node_modules_babel_preset_react_app_node_modules_babel_runtime_helpers_esm_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__["default"])(_ref, ["iconType"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_fortawesome_react_fontawesome__WEBPACK_IMPORTED_MODULE_2__["FontAwesomeIcon"], Object.assign({
    icon: iconType
  }, rest, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 3
    }
  }));
};

/***/ }),

/***/ "./components/Input.tsx":
/*!******************************!*\
  !*** ./components/Input.tsx ***!
  \******************************/
/*! exports provided: Input */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Input", function() { return Input; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);

var Input = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.input.withConfig({
  displayName: "Input",
  componentId: "sc-1w07j86-0"
})(["border:1px solid ", ";border-radius:4px;font-size:15px;min-height:46px;padding:6px 15px;width:100%;"], function (props) {
  return props.theme.color.borderGreyDark;
});

/***/ }),

/***/ "./components/Layout/CategorySideMenu/CategoriesList.tsx":
/*!***************************************************************!*\
  !*** ./components/Layout/CategorySideMenu/CategoriesList.tsx ***!
  \***************************************************************/
/*! exports provided: CategoriesList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriesList", function() { return CategoriesList; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _CategoryElement__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CategoryElement */ "./components/Layout/CategorySideMenu/CategoryElement.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Layout/CategorySideMenu/CategoriesList.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


 // -- TYPES

// -- MOCKS
var categoriesMock = [{
  label: 'Fizjoterapeuta',
  route: '/specialist/fizio'
}, {
  label: 'Psycholog',
  childrens: [{
    label: 'Uzależnienia',
    route: '/specialist/addiction'
  }, {
    label: 'Depresja',
    route: '/specialist/depression'
  }]
}, {
  label: 'Dermatolog',
  route: '/specialist/therma'
}]; // -- COMPONENT

var CategoriesList = function CategoriesList() {
  var renderCategoryItems = function renderCategoryItems(categoriesArray) {
    if (!Array.isArray(categoriesArray)) {
      return null;
    }

    return categoriesArray.map(function (singleCategory) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CategoryElement__WEBPACK_IMPORTED_MODULE_2__["CategoryElement"], Object.assign({}, singleCategory, {
        key: singleCategory.label,
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 50
        }
      }));
    });
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Wrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 55,
      columnNumber: 5
    }
  }, renderCategoryItems(categoriesMock));
}; // -- STYLED

var Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.ul.withConfig({
  displayName: "CategoriesList__Wrapper",
  componentId: "hsmwrk-0"
})(["padding:0;margin:30px -24px 0;list-style:none;border-bottom:1px solid ", ";"], function (props) {
  return props.theme.color.borderGreyDark;
});

/***/ }),

/***/ "./components/Layout/CategorySideMenu/CategoryElement.tsx":
/*!****************************************************************!*\
  !*** ./components/Layout/CategorySideMenu/CategoryElement.tsx ***!
  \****************************************************************/
/*! exports provided: CategoryElement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryElement", function() { return CategoryElement; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "../node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Layout/CategorySideMenu/CategoryElement.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




// -- COMPONENT
var CategoryElement = function CategoryElement(_ref) {
  var label = _ref.label,
      route = _ref.route,
      childrens = _ref.childrens;
  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_3__["useRouter"])();

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false),
      expandStatus = _useState[0],
      setExpandStatus = _useState[1];

  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    var isChildrenActive = Array.isArray(childrens) && childrens.find(function (singleChildren) {
      return singleChildren.route === router.asPath;
    });

    if (expandStatus !== !!isChildrenActive) {
      setExpandStatus(!!isChildrenActive);
    }
  }, [childrens]);

  var renderLabel = function renderLabel() {
    if (!route) {
      return label;
    }

    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
      href: route,
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 7
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }
    }, label));
  };

  var renderIndentItems = function renderIndentItems() {
    return Array.isArray(childrens) && childrens.map(function (_ref2) {
      var label = _ref2.label,
          route = _ref2.route;
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Item, {
        key: label,
        active: router.asPath === route,
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 5
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
        href: route,
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 53,
          columnNumber: 7
        }
      }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54,
          columnNumber: 9
        }
      }, label)));
    });
  };

  var handleExpand = function handleExpand() {
    !route && setExpandStatus(!expandStatus);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Item, {
    onClick: handleExpand,
    active: router.asPath === route,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 67,
      columnNumber: 7
    }
  }, renderLabel()), expandStatus && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Indent, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 9
    }
  }, renderIndentItems()));
}; // -- STYLED

var Item = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.li.withConfig({
  displayName: "CategoryElement__Item",
  componentId: "sc-12n2ix-0"
})(["padding:10px 24px;border-top:1px solid ", ";cursor:pointer;", ""], function (props) {
  return props.theme.color.borderGreyDark;
}, function (props) {
  return props.active && "\n    a {\n      color: ".concat(props.theme.color.primary, ";\n    }\n  ");
});
var Indent = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.ul.withConfig({
  displayName: "CategoryElement__Indent",
  componentId: "sc-12n2ix-1"
})(["padding:0;margin:0;list-style:none;li{padding-left:50px;}"]);

/***/ }),

/***/ "./components/Layout/CategorySideMenu/CategorySideMenu.tsx":
/*!*****************************************************************!*\
  !*** ./components/Layout/CategorySideMenu/CategorySideMenu.tsx ***!
  \*****************************************************************/
/*! exports provided: CategorySideMenu */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategorySideMenu", function() { return CategorySideMenu; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Card */ "./components/Card.tsx");
/* harmony import */ var _Input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Input */ "./components/Input.tsx");
/* harmony import */ var _CategoriesList__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./CategoriesList */ "./components/Layout/CategorySideMenu/CategoriesList.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Layout/CategorySideMenu/CategorySideMenu.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



 // -- COMPONENT

var CategorySideMenu = function CategorySideMenu() {
  var onFilterChange = function onFilterChange(e) {
    console.log(e.target.value);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Card__WEBPACK_IMPORTED_MODULE_1__["Card"], {
    header: "Wybierz kategori\u0119",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Input__WEBPACK_IMPORTED_MODULE_2__["Input"], {
    onChange: onFilterChange,
    defaultValue: "tezt",
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_CategoriesList__WEBPACK_IMPORTED_MODULE_3__["CategoriesList"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 7
    }
  }));
}; // -- STYLED

/***/ }),

/***/ "./components/Layout/GridElements.tsx":
/*!********************************************!*\
  !*** ./components/Layout/GridElements.tsx ***!
  \********************************************/
/*! exports provided: Row, Column */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Row", function() { return Row; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Column", function() { return Column; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
 // -- TYPES

// -- COMPONENTS
var Row = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "GridElements__Row",
  componentId: "sn6emi-0"
})(["display:flex;flex-wrap:wrap;margin:0 -15px;"]);

var generateWidth = function generateWidth(key, props) {
  var xl = props.xl,
      lg = props.lg,
      md = props.md,
      s = props.s,
      es = props.es;
  var selectedWidth = props[key] || es || s || md || lg || xl || 'auto';
  return typeof selectedWidth === 'number' ? "".concat(selectedWidth, "%") : selectedWidth;
};

var Column = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.div.withConfig({
  displayName: "GridElements__Column",
  componentId: "sn6emi-1"
})(["display:flex;padding:0 15px;", ""], function (props) {
  return "\n    @media (max-width: 576px) {\n      width: ".concat(generateWidth('es', props), ";\n    }\n\n    @media (min-width: 576px) {\n      width: ").concat(generateWidth('s', props), ";\n    }\n\n    @media (min-width: 768px) {\n      width: ").concat(generateWidth('md', props), ";\n    }\n\n\n    @media (min-width: 992px) {\n      width: ").concat(generateWidth('lg', props), ";\n    }\n\n    @media (min-width: 1200px) {\n      width: ").concat(generateWidth('xl', props), ";\n    }\n  ");
});

/***/ }),

/***/ "./components/Pages/Specialist/SpecialistList.tsx":
/*!********************************************************!*\
  !*** ./components/Pages/Specialist/SpecialistList.tsx ***!
  \********************************************************/
/*! exports provided: SpecialistList */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialistList", function() { return SpecialistList; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Card */ "./components/Card.tsx");
/* harmony import */ var _SpecialistListItem__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SpecialistListItem */ "./components/Pages/Specialist/SpecialistListItem.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Pages/Specialist/SpecialistList.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


 // -- MOCKS

var doctorsList = [{
  fullName: 'Gregory House',
  category: 'Urolog',
  specialisation: 'MDS - Periodontology and Oral Implantology, BDS',
  rating: 4.5,
  ratingsNumber: 12,
  recomendation: 67,
  pricing: '100-200',
  feedbacksNumber: 13,
  photo: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEBUQEA8VFRAVFRUXEBUPFRUPFRUVFRIXFhUVFhUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFy0dHR4tLS0rKy0tKy0rLS0tLS0tLSstLS0uLS0tKy0tLS0tLS0tLSstLSstLSstLS0tKy0tK//AABEIAPQAzgMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xAA+EAABAwIDBQYEBAYABgMAAAABAAIRAwQSITEFBkFRcRMiYYGRsQcyUsEUQqHRIzNicuHwJGOCkqKyFUNT/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAECAwQF/8QAJhEBAQACAgIBAwQDAAAAAAAAAAECEQMhEjFBBCJREzJCgXGRsf/aAAwDAQACEQMRAD8A6lVquxHvHU8TzSO1d9R9ShV+Y9T7pKhYrtXfUfUodq76nepSUECu2d9R9Sh2zvqPqUlFKBfau+o+pQ7V31O9Sm8SoN4t8Le0BBOOtwptyj+48ETJtpBUf9TvUpg7QZiwG4aHDUGoJHXNcQ2xv1dXRdFc06fBlEmmwdSM3FZcmtUcSC8NEZglpeSeJUbW8XoHa++tnbODat3JJh3ZO7TBH1wck7sze+zuC4Ub1pwx878AIPEEnNefWWz2mXAGeGbvMniUzIBP5fUQRnomzxemKG0mVJ7O5a+DBwVA7PlkVINR31O9SvNWzdpVaJBpVXNzkQ4jOZkrYbt/E59IilcDtRik1JJeATnM6gJtHi7J2rvqd6lDtXfU71KrtlbVpXNIVqD8TDlyIPIjmpeJSg92rvqPqUXau+o+pTUopQPds76j6lDtnfWfUpiUENHu2d9Z9Sh27vrPqUygge7d31n1KHbu+o+pTKCB4VnfU71Kk2b3Ys3HTmeYUOlqp1oO95fcKUIVZ3ed1PukSnKw7zup90mFCSZQlKhCECSf9Kx29W/QtH9myh2roMnFkOUK73mqVxRP4dgLoJLnkBjYGp59FxPat5VqVCa9XFU8IjyULSL29+IF1cAsB7MHUNH6YtVm64dUMuieZOZ81W3faNzYePBTLO5bUYMR74ydOSLT8FyxmRpvd4ADD+5RVb2oY/hPazgQ2cuicc92mGW82Ohw9dUhz3TDKpd4PyULG+zqOnC4eeR8wo93a1CO6Z5jUz9wnJc12dIh3AsPvKmU6JqafNzbkfRNmtqemS0CeeY4gcU5VkkhoGDInDxPirGrsqoGy9vOJUIjMiP9CbRcbPa03a3lr2LiaT4DnS9gzY7hmOGXFdt3c3kt7ym11J2F5GdN/ddI1w/UvPVQRmBp+qlbO2vWoFr6VR0teHYZhuRzCmK2PScIQo+yL9tzb07hmlRoJjg78w9VKhSoRCCVCEIEwglQhCBKCVCCA6Wqm2nzeX3ChUtVPtdfL7hShErDvHqfdIhO1R3j1PukwgRCEJeFCEGb38ruZY1SNIA8yuGuuWAS+A4HKfsuyfFqq5uzThMNNWmH+InQLg1pa1Li5bTGrjl4BUrTG9dLq3ip8uIjx0T9PYpLpBa0cZjNb7YO7bKdOC0HnInNWTN1KbjiDYPgsP1N+nZ+hJ7cyqWDxOES0cSISKWx6lQgMYcR5DLzXYaW6zPzEu65D0CsbXYzGaADop86jwx/LnOxtxX4f4tTXgM4WlsN1qNPMM/z1WuFuBwSSwKttq+PjPTH7a2HjY4AdI55rF3W6VRjTUfkToAJP+F2F1JNVaIggiQqy2el7McvcefNoWDqeROuscOpUO2E9Aumb7bIaKL3RBJ7oC5s4YQWjjIW+GXlHJy4eOTqvwPvy+ldUTMNqNczk0EQY6rpcLknwP2lFxWtcGVRgeHDgWA5HnIXXoWsc19m4QhLhFClBEIQlwihAmEUJcIkBU9VOtdfL9lCZqp1rr5fsgZqN7x6n3ScKfqNzPU+6LCiDWFDCncKGFBz/wCMt8KdgygWya9SGng3AMRPVcy3Gt/+NbzDSQt/8cQS2zbBw4qhJ4SQAM1kN2SGXYdzaAsuT1XRw+46faNyVtbmFU2j1YUqi5sXfnNrAFGo7KidBWkrC4lFIKUkEJUwTkw9OPTLyq1eRn97bMPtn5AkCR4Ljd9QhxHBd5u6YcwtOhBC5Dt+0w1XUyIj/Qr8V70z5p1sfwwujT2vbDOHl7XQY+ZhAnmvQLm5rzRsa5NC6o1ge8yqw5ju6xn5FenaozkaHP1XRHDlEeEWFO4UMKlUzhQwp3CiwoGoRYU9hRYUDTW5qbbDPyUcNUu3GfkpSDm5nqUWBSC3NDCiqPgR4E/hQwoMD8Y7QO2Z2v56VVhadcnGD+i5Jse4w1mEnUj9V3L4m2YqbJuQTGFuPP8ApXALV+F1I8nAfss823E7HZvkSrBhVZYtgADw9laBzW5ucFx4x6dqXQUgjxVadoUgf5g8zCcpbQpu+V4J8CtJ0zstTkeSiOucv2VPtPbb2D+G2TwJzU7h4VoakKK57TlKwFbbV453eqtaOTgAfQZqVQuCe8a0n+gGPNRSY1q6h5aLE7/7PyFy0ZiA7otDaVHkZOB8ckvblp2trUZxLSR1CrLq7TlNzTlOyLQ1bumxoHzg98Ymw3PMcQvQOxb01cbXOxFhGYGEdAuUfDW0DrirUcO8xgDZ4EkyupbpW8U6jz+d5joMlvLfPTnywxnDcr7W+FFhTpCKFu4TWFDCnYRQiTWFDCnYRQgawqTQGfkmoUiiEQdIRQlFBAmEISkEFLvfTLrKoMIcI7wdmC3iIXn3eDZbrciB3HOa+kfDEJHkvTNakHtcw6OBB8wuPb77Ni3cxw71F+X9pd+yxz6y3+XZwSZcVnzO1hWrObSYWDNzRmeHdVRd3FQ5Y8+MA+uZgLRU6U06enyN9lEr7JY/+YO6D+XIf5XPvTs+GGva1EuIdWqlwmRTiMgSddfJTrOo6lEY+B7+sHMHgtHdbEtnOL3mXHLKdOgUqps5tUh7mkxGbssmiAFfK46Uwxyl3dLTYxL6WevGVA2lbHPD+iuNnU4bCKtTE5rPXTTfbE3WwhUouEHt8YLZzYWxmD4+KlbF3aLGEPwgnDDm917cPIgDVakWAObSQlMs3fVPVX8rrTO4Y78vlEtbXCIB6zGfopbmZR5KQKEcc0l7PX3VdLWsZulaOo3NyY7jnho66wuq7OpYaTG+Enqc1lLK0xYcIiXn1JglbOIy5ZLfindrk+qsmOOMBEggt3GJBBEUARIIkAT9JMSn6KB4okCiQGgilFKBSzG+OzmubjIGFwwvnnwJWllIrU2vaWOEtcIKrljuaacXJ4ZTJg6NGA1vIAemilttmnUSfFIuu7ULIiMo6aIzXhcfqvTnc6Oi0aNGhIuRoEivdwJUW3eS6X+Xgpq0xvurCwOZ8Uq51TFuYepFyQc5yUydK/JIr4R+6epVwVX9u1xyzEJgswulh6g8eielvGVcvfyTFR+Sa7fuyeAUWm/EMQ4hRWdmlru9buOF5+QFx85K0WJVmwXf8OweLvdTsS6sJqPP5crll38HJRSkFyLErsjkopSMSLEgWSilILkkvQOyn6BUMuT9s7PyQSiUUpDjmilAuUJTcoi5A4XIsSbJRSgp96aDezFYDvNcMRHFpyzWXbX/AIgaeUrd3FEVKbqZ0c0j9iue3THMeC75qbsL/b/K5ubHV27fps9zX4SLqsARPko9euS3JKuGd4yfFp1BB0hUjttim/BVt6jWE5VSAGHrxCzkdm9pdvtCtRPyl7DpJ7w6Kbc7Uc8dm1hExiLvZWTdk1C8NAYQXAAzIwuEh2mYVhS3fMvaagBaO4QJBJE8VeY38M7zcc/kz9tTcPAKLta/ZRcxjqzRVd8rSe9HMxotBevpUsbWHtKmBmETLWvk4pI6aeKprbd9kPqVADWqRidAkAaDolmp2tjl5d61P+rKjUxUzOuE+PBNWE9k2eSS54pUnNbxGEE+OSFKIaznAJ65KknalvVrUbGqfwgOOZ9Sp0rN2d6KV4LZzodh7gOWJsajmtPg5LvuHj08y5buyJRSidlqk4lULlFKRiQxIFSkyiLkkvQKJUmzOfkoRepVg7PyQTHHNJJSHuzPUpBeiDkopTZcklyB0uRYkyXIsSkPB6zW9tif57RIMCtHCMg77K+a5PhgiCJByIPEclXLHymlsMrjdxzaneyMB1GbD9lZtw1KeFwBB1BzUTeDZgo1iGTgObJ4SdOii7NuzpPHPnK5bjp6OHJ8xLpWRpvD6dZ7SMMQ4kQ2cIg5RmU9XDnxjrvJGsEsnOc4RVmFwyOqY/DvLsipmVb7x35WTf8AhNoYGwBBP35lP1qmWvRN0aWESdfuo9zWkRx8FHdUyy+aYqHEQCcpkkZp2mzE4AaSA2OcqvovkkcTr4AceqvtgW2Kq0cG94+WiSfdpl5axtvwTv8AbKNSlTuKeVxbOa+k4axkHN6EStPsS/bXotqsOThJHEHiCivaeJrgdCFn9yKJZWqMkhrQ7LgZIg/qvUuMuG/w8mXVa99MEZqObTk6OolTYRQudogus3cwVHqNI1CtkTtNE0bU+NILlZ1LRruEHmFAr2bxpmPDX0ROzBcpmzXZnp9wq98jUEdclK2U7vHp9wiE2o7M9T7psuSKru8ep90guQO4kWJNYkUoHC5FJ4BLoUsWZ0UpvggYtWcSOikpQRIMvvLQxsc6PkcQ7waQM/1WODMD9e6dfAxC6CHDtq1MiRDHEHiHYmn2WW21sg0Zc0F1A8dSzwd4ciqcvHf3T+2/ByT9l/oyy7wgYtDxGaf/APkOXqqTAdWOy4cQkOa8/my8Fz7jtm51peVL2RyTDy4juuE+BmOah29DKXOJj/eGqkte1rSflaB0UeRcbSmU2sbOf3J/dbXYNgaVKXiKj83+A4NVXu3smSLis2P/AMWHgOD3DmtKunh49fdXH9Tyy/Zj6hFQZKl3fEXNdvER6ZK8cqO07t+/k4AHzH+F14942OO+406NBqELnXEQkPdwCdSMAQE0IJSCBDmg6gHqmqNkwOlojLh1T8JVPVBT1fmPU+6QlPEvIH1H3UujbgGdSi20anbuPgl/h84/VTECc1KNiY0AQOCJrc0TjGacp80QXCIBKRDVQlSXdPDdCr+V7BTPUOc5v3UkDgdOXBHtugTRcW/M0h7f+gz+6Km6QCNCJC2x9M77UW091mvOO3cKb+LCJpu8vyrM3FF9JxbWpljvHvNPiHDJdIYJWQ3r32pUXG3ptxvGT6lRhdRZzAP5issuCZ3r26OP6nLD33FDUrNaJLhHD9lfbtbCNRwr12wwZ0qZ4/1O+wWSvt/aFJo/DWrKlWZcXM7OlMySBMytbuv8RbK6Ip1D+Gr/AEViAxx/ofx6KuP0/hd5dtOX6u546xmm1CNAD04EZg9CgtnIIhUlvRxXj3cj/wCrQPcq8CjbHt8nVDq4ujpiKnepTXaypGQEtIpCMktYrAgUaJAkII0EBJTAiSmIK6hSh5J4kx6p96U4Z+aDgkTSQg/JGE3WOYCn5QKronWCAmjm4BSEoCBQCMhQCLZ6KosmYQaZ1YSPLVv6EK5VPtet2dRpb89XuMnTG0EifKfRaYX4VyZT4gbwvpsdZWr8NxUYcdQf/WCIDfBx58Fj91976b2Cy2kwAt7rakR5OC01/sB+Nzn5ucSXO1JJWA362G63rMrRLK0gn+tsT6ghb6mMRO2h3l3Fws/EWhx0yJgZ5a5FZndbdj8bdYXiKFLOrPE8GforDdbe6taFtP8AmUHkNNN0kjEQJaunbK2KKeIsAbjOIxzPNX6s7V7nRNKvXt4bSeHNH5HZgAcByV/ZbRD2g1GGm7k7MeRUehYAa5lTK9MFhaRkRBWWVlWm0hw0A46KWxsCBoqnYVrhxEOdh0a0mQOcK4hY5+9LQUJSEJivchjS4gkAEmOQEqiT6CzO7e+DL7tOxoPaGRnVgYpJAIA4ZK+BedTHRToPkIkgU+ZlKCgGlMSUpigM1BqiCWdU2MipB4UxU1UhMO1UwY7fDeypaXdG3oUm1KlXCyHEiC8zIgGYAlWde/ugPma0j5iGyJ5DmsnTZ2u8j3VO92LKjqY4BzWsa0+jj6rogthhz1VoVSC9uS3KoZ/tHstRSBDRJkwJPM8UxbW4GcZ8FIVbQoKu2/a46JIHfpubUp9WGf1EjzVgjIlRKKSq0OGLmJXP/ia01LMMYwueK1PBHAl0GV0Mswks5HLodFl9t1mUg59QSGQ9s55g5fqV049qMVtPc9lhbUr2tcTXY5r+yiAYzw85XSNgbWZXosrUzLHiR4cwVxzejb1S8dL/AJB8o4dVofhNtUtc60ecj36PgfzD7rTWvtR37dat6klDaFTDTJ45AdSmbaQ8g6FP16WN9NnAOxO6NWF9pWFlRwsA4xn1KfQCCx2uCZvKZdTeGiXFrgBzJBgJ5AIMD8K7ZzPxLajC17DTY5rhBBazMfqt4qjYW1GXLq1ak0dmKppB4EGoaeRceYmQPAK3SgFEEbkGoDRsRI2KA07im6qcfqkP0Ug6bpCaeM0dA5kJVQKfkZy12Cynd1bkOxVbhxAJH8tgAlo8SVoHNUc/M3+53sFKKkHRdGSeKivT1CriHiNVUOIIIIIG0GQ4O4Hun3C5T8VtpBpbbA5/NU6ZYQuw3FHG0t48Oo0XmvfW/NfaNwZ0fgHhgAELfiyVsV1J0jPRdE+HWxuzo1doVhhbEUcWWQOblR7hbqm8qS/K2pmah+siO6r/AOI+8Qw/gbbusAh2DIARotsZ3r/atrotpVDnA82tcPRWdk2XF3kPusXuhf46Fs6ZJogHqMlu7WnhaB6rDl6WxPI0SCwWGsn8UdpPo7Lrmk7DUeOzBGo7TumPGFqKj+A1WE+Lb4sqbToawc7pTpvf7tCmQWPw0tez2ZQA44nep/wtWFU7p0MFjbN/5FM/9zAT7q3ShLkYRI0ASmJKNqgNYpJCS7kjqDPLVIxeqsGg6HA81JcMlDrHRS6RkKaI7h3h1PsE8m6wgjqPunYyQJIUK5eWuZhOeIk+IAj7qaSmH05eJ4DLzQTKb5CUo7cs0+0yqhTSuH787oOG2i2i2Kd2O0BzhpbArZ9YPmu3hZ/f2m8WT69Fs1qIxN54SQKgHln/ANKvx3WURfTCbd27TsbcWNr80ZkcyIJPiub1ahJJJlx1JTdxeOe4vLpc7Mk56py2oOqOaxjcT3GGjxXVlf44qyfNdF+Er3VcFLhTc4nwbwXYQsL8MN2vwjar3GXuwh3g6JdC3S5uW7vXwtjARPKMlIWaSAFz34yEmhSYBJcKuQ/tDZ/810Rct+Mtf+JaU8UYsQ1icVakPspg6bs+nho0mfTTYPRoCfJSaY7oHJo9kZUAAI0EEAQZqiQYc0CS3MpD2I0FIYq0wnqDckEEoFwwR/vNLDUEFAQ5iGAYj/vBBBAtrUTGwUSCJPQgaYcC1wlpBBB4giCEaCgeea26tFleqxr6mFtR+EEtyGLT5dFt9wN36NNlS4Ac6qMmueQcIz0ACCC3xt8VK3u7jItmGSS6XOJ4kkyrOEEFjl7q09EwhhRoIEvGSrNq7PpVKeKpSa9w+UvAdHHLlmAggkE+hm0dAnA1GggEIQgggEImDNBBB//Z'
}, {
  fullName: 'Klara Schemetrling',
  category: 'Okulista',
  specialisation: 'BDS, MDS - Oral & Maxillofacial Surgery',
  rating: 3.5,
  recomendation: 51,
  pricing: '50-150',
  feedbacksNumber: 3,
  ratingsNumber: 12,
  photo: 'https://demo.cherrytheme.com/gems/wp-content/uploads/2018/11/our-team-04.jpg'
}]; // -- COMPONENT

var SpecialistList = function SpecialistList() {
  if (!Array.isArray(doctorsList) || !doctorsList.length) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Card__WEBPACK_IMPORTED_MODULE_1__["Card"], {
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 35,
        columnNumber: 12
      }
    }, "No specialist available");
  }

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, doctorsList.map(function (singleDoctor) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_SpecialistListItem__WEBPACK_IMPORTED_MODULE_2__["SpecialistListItem"], Object.assign({
      key: singleDoctor.fullName
    }, singleDoctor, {
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 41,
        columnNumber: 9
      }
    }));
  }));
};

/***/ }),

/***/ "./components/Pages/Specialist/SpecialistListItem.tsx":
/*!************************************************************!*\
  !*** ./components/Pages/Specialist/SpecialistListItem.tsx ***!
  \************************************************************/
/*! exports provided: SpecialistListItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpecialistListItem", function() { return SpecialistListItem; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ "@fortawesome/free-regular-svg-icons");
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _Card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../Card */ "./components/Card.tsx");
/* harmony import */ var _Avatar_Avatar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../Avatar/Avatar */ "./components/Avatar/Avatar.tsx");
/* harmony import */ var _Avatar_constants_size__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../Avatar/constants/size */ "./components/Avatar/constants/size.ts");
/* harmony import */ var _Avatar_constants_shape__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../Avatar/constants/shape */ "./components/Avatar/constants/shape.ts");
/* harmony import */ var _Heading__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../Heading */ "./components/Heading.tsx");
/* harmony import */ var _Typography__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../Typography */ "./components/Typography.tsx");
/* harmony import */ var _Button_Button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../Button/Button */ "./components/Button/Button.tsx");
/* harmony import */ var _Button_constants_size__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../Button/constants/size */ "./components/Button/constants/size.ts");
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../Icon */ "./components/Icon.tsx");
/* harmony import */ var _RatingStars__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../RatingStars */ "./components/RatingStars.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/Pages/Specialist/SpecialistListItem.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;












 // -- TYPES

// -- COMPONENT
var SpecialistListItem = function SpecialistListItem(_ref) {
  var fullName = _ref.fullName,
      specialisation = _ref.specialisation,
      category = _ref.category,
      ratingsNumber = _ref.ratingsNumber,
      recomendation = _ref.recomendation,
      feedbacksNumber = _ref.feedbacksNumber,
      pricing = _ref.pricing,
      photo = _ref.photo,
      rating = _ref.rating;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Card__WEBPACK_IMPORTED_MODULE_3__["Card"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 3
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(CardContentWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DoctorDetailsLeft, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ImageWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Avatar_Avatar__WEBPACK_IMPORTED_MODULE_4__["Avatar"], {
    image: photo,
    shape: _Avatar_constants_shape__WEBPACK_IMPORTED_MODULE_6__["Shape"].ROUNDED,
    size: _Avatar_constants_size__WEBPACK_IMPORTED_MODULE_5__["Size"].XXL,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47,
      columnNumber: 11
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Details, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Heading__WEBPACK_IMPORTED_MODULE_7__["Heading"], {
    level: 5,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 11
    }
  }, fullName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Specialisation, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57,
      columnNumber: 11
    }
  }, specialisation), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Category, {
    level: 5,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60,
      columnNumber: 11
    }
  }, category), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(RatingsWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 63,
      columnNumber: 11
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_RatingStars__WEBPACK_IMPORTED_MODULE_12__["RatingStars"], {
    rating: rating,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 64,
      columnNumber: 13
    }
  }), " ", "(".concat(ratingsNumber, ")")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DoctorDetailsRight, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 69,
      columnNumber: 7
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DetailWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 70,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyledIcon, {
    iconType: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__["faThumbsUp"],
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 71,
      columnNumber: 11
    }
  }), " ", recomendation, "%"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DetailWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 73,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyledIcon, {
    iconType: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__["faComment"],
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 74,
      columnNumber: 11
    }
  }), " ", feedbacksNumber, " Opinii"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(DetailWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(StyledIcon, {
    iconType: _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_2__["faMoneyBillAlt"],
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77,
      columnNumber: 11
    }
  }), " ", pricing, "z\u0142"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ActionButton, {
    outline: true,
    size: _Button_constants_size__WEBPACK_IMPORTED_MODULE_10__["Size"].LARGE,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79,
      columnNumber: 9
    }
  }, "Zobacz profil"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ActionButton, {
    size: _Button_constants_size__WEBPACK_IMPORTED_MODULE_10__["Size"].LARGE,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 9
    }
  }, "Zarezerwuj wizyt\u0119"))));
}; // -- STYLED

var CardContentWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "SpecialistListItem__CardContentWrapper",
  componentId: "sc-1cek3oo-0"
})(["display:flex;width:100%;@media only screen and (max-width:767px){flex-direction:column;text-align:center;}"]);
var Category = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Heading__WEBPACK_IMPORTED_MODULE_7__["Heading"]).withConfig({
  displayName: "SpecialistListItem__Category",
  componentId: "sc-1cek3oo-1"
})(["font-size:14px;color:#20c0f3;"]);
var Specialisation = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Typography__WEBPACK_IMPORTED_MODULE_8__["Paragraph"]).withConfig({
  displayName: "SpecialistListItem__Specialisation",
  componentId: "sc-1cek3oo-2"
})(["margin-top:0px;font-size:14px;color:", ";"], function (props) {
  return props.theme.color.muted;
});
var RatingsWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "SpecialistListItem__RatingsWrapper",
  componentId: "sc-1cek3oo-3"
})(["font-size:14px;font-family:'poppinsmedium';display:flex;align-items:center;"]);
var DetailWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Typography__WEBPACK_IMPORTED_MODULE_8__["Paragraph"]).withConfig({
  displayName: "SpecialistListItem__DetailWrapper",
  componentId: "sc-1cek3oo-4"
})(["margin-top:0;font-size:14px;"]);
var ActionButton = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Button_Button__WEBPACK_IMPORTED_MODULE_9__["Button"]).withConfig({
  displayName: "SpecialistListItem__ActionButton",
  componentId: "sc-1cek3oo-5"
})(["font-size:14px;font-family:'poppinsmedium';margin-bottom:10px;text-transform:uppercase;"]);
var StyledIcon = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Icon__WEBPACK_IMPORTED_MODULE_11__["Icon"]).withConfig({
  displayName: "SpecialistListItem__StyledIcon",
  componentId: "sc-1cek3oo-6"
})(["margin-right:10px;"]);
var DoctorDetailsLeft = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "SpecialistListItem__DoctorDetailsLeft",
  componentId: "sc-1cek3oo-7"
})(["display:flex;@media only screen and (max-width:767px){flex-direction:column;}"]);
var DoctorDetailsRight = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(DoctorDetailsLeft).withConfig({
  displayName: "SpecialistListItem__DoctorDetailsRight",
  componentId: "sc-1cek3oo-8"
})(["margin-left:auto;flex:0 0 200px;flex-direction:column;max-width:200px;@media only screen and (max-width:767px){text-align:center;flex:0 0 100%;max-width:100%;margin-left:0;}"]);
var ImageWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "SpecialistListItem__ImageWrapper",
  componentId: "sc-1cek3oo-9"
})(["flex:0 0 150px;margin-right:20px;width:150px;@media only screen and (max-width:767px){margin:0 auto 20px;}"]);
var Details = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "SpecialistListItem__Details",
  componentId: "sc-1cek3oo-10"
})(["display:flex;flex-direction:column;margin-bottom:15px;@media only screen and (max-width:767px){align-items:center;text-align:center;}"]);

/***/ }),

/***/ "./components/RatingStars.tsx":
/*!************************************!*\
  !*** ./components/RatingStars.tsx ***!
  \************************************/
/*! exports provided: RatingStars */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingStars", function() { return RatingStars; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Icon */ "./components/Icon.tsx");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ "@fortawesome/free-solid-svg-icons");
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__);
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/components/RatingStars.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



 // -- TYPES

// -- COMPONENT
var RatingStars = function RatingStars(_ref) {
  var rating = _ref.rating;

  var renderStars = function renderStars(value) {
    var fullStarsNumber = Math.floor(value) || 0;
    var fullStars = new Array(fullStarsNumber).fill('').map(function (singleStar, index) {
      return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Icon__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
        key: "star-".concat(index),
        iconType: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faStar"],
        __self: _this,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16,
          columnNumber: 86
        }
      });
    });
    var halfStar = value % 1 ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(HalfStar, {
      iconType: _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_3__["faStarHalf"],
      __self: _this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 34
      }
    }) : null;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, " ", fullStars, " ", halfStar, " ");
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Wrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GoldenStarWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }
  }, renderStars(rating)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(GreyStarWrapper, {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 7
    }
  }, renderStars(5 - rating)));
}; // -- STYLED

var Wrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.div.withConfig({
  displayName: "RatingStars__Wrapper",
  componentId: "ynxjzy-0"
})(["display:flex;margin-right:5px;"]);
var GoldenStarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.span.withConfig({
  displayName: "RatingStars__GoldenStarWrapper",
  componentId: "ynxjzy-1"
})(["svg{color:", ";}"], function (props) {
  return props.theme.color.gold;
});
var GreyStarWrapper = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.span.withConfig({
  displayName: "RatingStars__GreyStarWrapper",
  componentId: "ynxjzy-2"
})(["transform:scaleX(-1);margin-left:-18px;svg{color:", ";}"], function (props) {
  return props.theme.color.borderGreyDark;
});
var HalfStar = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_Icon__WEBPACK_IMPORTED_MODULE_2__["Icon"]).withConfig({
  displayName: "RatingStars__HalfStar",
  componentId: "ynxjzy-3"
})(["margin-left:-4.5px;"]);

/***/ }),

/***/ "./components/Typography.tsx":
/*!***********************************!*\
  !*** ./components/Typography.tsx ***!
  \***********************************/
/*! exports provided: Highlight, Del, S, Additional, Underline, Small, Bold, Italic, Monospace, Paragraph, Link */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Highlight", function() { return Highlight; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Del", function() { return Del; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return S; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Additional", function() { return Additional; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Underline", function() { return Underline; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Small", function() { return Small; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Bold", function() { return Bold; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Italic", function() { return Italic; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Monospace", function() { return Monospace; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Paragraph", function() { return Paragraph; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Link", function() { return Link; });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
 // -- STYLED

var Highlight = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.mark.withConfig({
  displayName: "Typography__Highlight",
  componentId: "mlbp27-0"
})(["padding:3px;background-color:", ""], function (props) {
  return props.theme.color.highlight;
});
var Del = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.del.withConfig({
  displayName: "Typography__Del",
  componentId: "mlbp27-1"
})(["text-decoration:line-through;"]);
var S = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.s.withConfig({
  displayName: "Typography__S",
  componentId: "mlbp27-2"
})(["text-decoration:line-through;"]);
var Additional = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.ins.withConfig({
  displayName: "Typography__Additional",
  componentId: "mlbp27-3"
})(["text-decoration:underline;"]);
var Underline = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.u.withConfig({
  displayName: "Typography__Underline",
  componentId: "mlbp27-4"
})(["text-decoration:underline;"]);
var Small = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.small.withConfig({
  displayName: "Typography__Small",
  componentId: "mlbp27-5"
})(["font-size:80%;"]);
var Bold = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.b.withConfig({
  displayName: "Typography__Bold",
  componentId: "mlbp27-6"
})(["font-weight:bolder;"]);
var Italic = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.em.withConfig({
  displayName: "Typography__Italic",
  componentId: "mlbp27-7"
})(["font-style:italic;"]);
var Monospace = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.p.withConfig({
  displayName: "Typography__Monospace",
  componentId: "mlbp27-8"
})(["font-family:SFMono-Regular,Menlo,Monaco,Consolas,\"Liberation Mono\",\"Courier New\",monospace!important;margin-bottom:0;"]);
var Paragraph = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.p.withConfig({
  displayName: "Typography__Paragraph",
  componentId: "mlbp27-9"
})(["margin-bottom:16px;"]);
var Link = styled_components__WEBPACK_IMPORTED_MODULE_0___default.a.a.withConfig({
  displayName: "Typography__Link",
  componentId: "mlbp27-10"
})(["text-decoration:none;background-color:transparent;cursor:pointer;"]);

/***/ }),

/***/ "./pages/specialist/[spec].tsx":
/*!*************************************!*\
  !*** ./pages/specialist/[spec].tsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/Layout/GridElements */ "./components/Layout/GridElements.tsx");
/* harmony import */ var _components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/Layout/CategorySideMenu/CategorySideMenu */ "./components/Layout/CategorySideMenu/CategorySideMenu.tsx");
/* harmony import */ var _components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Pages/Specialist/SpecialistList */ "./components/Pages/Specialist/SpecialistList.tsx");
var _this = undefined,
    _jsxFileName = "/home/przemyslaw/Pulpit/moje/randlab/alfacare/client/pages/specialist/[spec].tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




 // -- COMPONENT

var Specialist = function Specialist() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Row"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 3
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Column"], {
    xl: 25,
    lg: 33.3,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout_CategorySideMenu_CategorySideMenu__WEBPACK_IMPORTED_MODULE_3__["CategorySideMenu"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(ResultColumn, {
    xl: 75,
    lg: 66.6,
    md: 100,
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Pages_Specialist_SpecialistList__WEBPACK_IMPORTED_MODULE_4__["SpecialistList"], {
    __self: _this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 7
    }
  })));
}; // -- STYLED


var ResultColumn = styled_components__WEBPACK_IMPORTED_MODULE_1___default()(_components_Layout_GridElements__WEBPACK_IMPORTED_MODULE_2__["Column"]).withConfig({
  displayName: "spec__ResultColumn",
  componentId: "sc-1vlbw7p-0"
})(["flex-direction:column;"]);
/* harmony default export */ __webpack_exports__["default"] = (Specialist);

/***/ }),

/***/ 4:
/*!*******************************************!*\
  !*** multi ./pages/specialist/[spec].tsx ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/przemyslaw/Pulpit/moje/randlab/alfacare/client/pages/specialist/[spec].tsx */"./pages/specialist/[spec].tsx");


/***/ }),

/***/ "@fortawesome/free-regular-svg-icons":
/*!******************************************************!*\
  !*** external "@fortawesome/free-regular-svg-icons" ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/free-regular-svg-icons");

/***/ }),

/***/ "@fortawesome/free-solid-svg-icons":
/*!****************************************************!*\
  !*** external "@fortawesome/free-solid-svg-icons" ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ "@fortawesome/react-fontawesome":
/*!*************************************************!*\
  !*** external "@fortawesome/react-fontawesome" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "prop-types":
/*!*****************************!*\
  !*** external "prop-types" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types");

/***/ }),

/***/ "prop-types-exact":
/*!***********************************!*\
  !*** external "prop-types-exact" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("prop-types-exact");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),

/***/ "url":
/*!**********************!*\
  !*** external "url" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("url");

/***/ })

/******/ });
//# sourceMappingURL=[spec].js.map