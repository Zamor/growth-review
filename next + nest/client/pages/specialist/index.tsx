import React, { ReactElement } from 'react'
import styled from 'styled-components'

import { Column, Row } from '../../components/Layout/GridElements'
import { CategorySideMenu } from '../../components/Layout/CategorySideMenu/CategorySideMenu'
import { SpecialistList } from '../../components/Pages/Specialist/SpecialistList'

// -- COMPONENT
const Specialist = (): ReactElement => (
  <Row>
    <Column xl={25} lg={33.3} md={100}>
      <CategorySideMenu />
    </Column>
    <ResultColumn xl={75} lg={66.6} md={100}>
      <SpecialistList />
    </ResultColumn>
  </Row>
)

// -- STYLED
const ResultColumn = styled(Column)`
  flex-direction: column;
`

export default Specialist
