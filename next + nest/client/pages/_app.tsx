import React, { FC } from 'react'
import { ThemeProvider } from 'styled-components'
import type { AppProps } from 'next/app'

import ky from 'ky-universal'

import BaseStyles from '../components/themes/BaseStyle'
import { theme } from '../components/themes/theme'
import { Layout } from '../components/Layout/Layout'

const MyApp: FC<AppProps> = ({ Component, pageProps }: AppProps) => (
  <>
    <BaseStyles />
    <ThemeProvider theme={theme}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </ThemeProvider>
  </>
)

export default MyApp

// const HomePage = () => {
//   const handleGetClick = async () => {
//     try {
//       const results = await ky.get('/test-connection').json()
//       console.log('get result', results)
//     } catch (e) {
//       console.log(e)
//     }
//   }

//   const handlePostClick = async () => {
//     try {
//       const results = await ky.post('/test-connection/12', { json: { companyName: 'randlab' } }).json()
//       console.log('post result', results)
//     } catch (e) {
//       console.log(e)
//     }
//   }

//   return (
//     <div>
//       Hello randlab
//       <button onClick={handleGetClick}> Test get Connection </button>
//       <button onClick={handlePostClick}> Test post Connection </button>
//     </div>
//   )
// }

// export default HomePage
