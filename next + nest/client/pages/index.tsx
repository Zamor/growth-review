import React from 'react'

import { CategorySideMenu } from '../components/Layout/CategorySideMenu/CategorySideMenu'
import { Row, Column } from '../components/Layout/GridElements'

// -- COMPONENT
const Main = () => (
  <Row>
    <Column xl={25} lg={33.3} md={100}>
      <CategorySideMenu />
    </Column>
    <Column xl={75} lg={66.6} md={100}>
      Tutaj będzie content strony głównej
    </Column>
  </Row>

)

export default Main
