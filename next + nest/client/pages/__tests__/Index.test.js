// -- CORE
import React from 'react'
import { describe, expect, it } from '@jest/globals'
import { render, screen } from '@testing-library/react'
// -- COMPONENT
import HomePage from '../index'

describe('HomePage component', () => {
  it('Homepage should contain "Hello randlab string"', () => {
    const testString = 'Hello randlab'

    render(<HomePage />)

    expect(screen.getByText(testString)).toBeDefined()
  })
})
