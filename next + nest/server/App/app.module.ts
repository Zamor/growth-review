import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod
} from '@nestjs/common'
import {
  NextModule,
  NextMiddleware
} from '@nestpress/next'

import { SimpleRequest } from '../SimpleRequest/simpleRequest.module'

import { NextRoutes } from '../middleware/nextRoutes.middleware'

@Module({
  imports: [
    // register NextModule
    NextModule,
    SimpleRequest
  ],
  controllers: []
})
export class AppModule implements NestModule {
  configure (consumer: MiddlewareConsumer) {
    // handle scripts
    consumer
      .apply(NextMiddleware)
      .forRoutes({
        path: '_next*',
        method: RequestMethod.GET
      })

    // handle assets
    consumer
      .apply(NextMiddleware)
      .forRoutes({
<<<<<<< HEAD
        path: 'public/*',
=======
        path: 'static/*',
>>>>>>> c7438daf81f71e13a64a470da6e27fac25ba5b70
        method: RequestMethod.GET
      })

    consumer
      .apply(NextRoutes)
      .exclude(
        'api/*'
      )
      .forRoutes({
        path: '*',
        method: RequestMethod.GET
      })
  }
}
