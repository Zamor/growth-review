import { Module } from '@nestjs/common'
import { SimpleRequestService } from './simpleRequest.service'
import { SimpleReqController } from './simpleRequest.controller'

@Module({
  controllers: [SimpleReqController],
  providers: [SimpleRequestService]
})
export class SimpleRequest {}
