import { Injectable } from '@nestjs/common'

@Injectable()
export class SimpleRequestService {
  getSimpleGet (): string {
    return 'Get is working'
  }

  getSimplePost (id: string, companyName: string): any {
    return {
      message: `Recived id from string ${id}`,
      companyName
    }
  }
}
