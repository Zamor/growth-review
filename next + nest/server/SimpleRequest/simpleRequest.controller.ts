import { Controller, Get, Post, Param, Body } from '@nestjs/common'
import { SimpleRequestService } from './simpleRequest.service'

@Controller('test-connection')
export class SimpleReqController {
  constructor (private reqService: SimpleRequestService) { }

  @Get()
  getTestRequest (): string {
    return this.reqService.getSimpleGet()
  }

  @Post('/:id')
  postTestRequest (@Param('id') id: string, @Body('companyName') companyName: string): string {
    return this.reqService.getSimplePost(id, companyName)
  }
}
