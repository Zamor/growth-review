import {
  IncomingMessage,
  ServerResponse
} from 'http'

import {
  Injectable,
  NestMiddleware,
  Req,
  Res
} from '@nestjs/common'

import { NextService } from '@nestpress/next'

@Injectable()
export class NextRoutes implements NestMiddleware {
  constructor (
    private readonly next: NextService
  ) { }

  async use (@Req() req: IncomingMessage, @Res() res: ServerResponse) {
    const url = req.url || '/'

    await this.next.render(url, req, res)
  }
}
