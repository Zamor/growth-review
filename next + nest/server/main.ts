import { NestFactory } from '@nestjs/core'
import { NextModule } from '@nestpress/next'
import { AppModule } from './App/app.module'

const conf = require('../client/next.config')
const nextAppConfig = {
  dev: process.env.BABEL_ENV !== 'production',
  dir: './client',
  conf
}

async function bootstrap () {
  const app = await NestFactory.create(AppModule)

  app.get(NextModule).prepare(nextAppConfig).then(() => {
    app.listen(3000)
  })
}

bootstrap()
